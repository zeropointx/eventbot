﻿using Common;
using Common.Config;
using CommonLibrary.Database;
using Org.BouncyCastle.Asn1.X9;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TS3Client;
using TS3Client.Commands;
using TS3Client.Full;
using TS3Client.Messages;
using static Common.CommandManager;
using static Common.EventFetcher;
using static Common.EventScheduler;
using static Common.PermissionManager;

namespace Teamspeak.NewClient
{
    public class TeamspeakWrapper
    {
        public class NewTeamspeakMessageData : MessageData
        {
            public override UserData getUserData()
            {
                UserData userData = null;
                userData = Common.PermissionManager.get().getUserByTsUid(senderUid);
                return userData;
            }

            public async override Task sendChannelResponse(string message)
            {
                if (platform.platformType != BotPlatform.PlatformType.Teamspeak)
                    return;
                TeamspeakWrapper tstelnet = (TeamspeakWrapper)platform.platform;
                tstelnet.sendChannelMessage(message);
            }

            public async override Task sendPrivateResponse(string message)
            {
                if (platform.platformType != BotPlatform.PlatformType.Teamspeak)
                    return;
                TeamspeakWrapper tstelnet = (TeamspeakWrapper)platform.platform;
                tstelnet.sendPrivateResponse(message, senderId);
            }
        }
        public struct TeamspeakClient
        {
            public string clid;
            public string id;
            public string databaseId;
            public string nickname;
            public string type;
            public string uid;
            public List<int> groups;
            public string countryCode;
        }
        public struct TeamspeakGroup
        {
            public string sgid;
            public string name;
            public string type;
            public string iconid;
            public string savedb;
            public override string ToString()
            {
                return $"ID: {sgid} Name: {name}";
            }
        }
        DateTime latestChannelNameUpdateTime = new DateTime(0);
        DateTime latestChannelDescUpdateTime = new DateTime(0);
        List<string> logLines = new List<string>();
        private static readonly ECKeyGenerationParameters keyGenParams = new ECKeyGenerationParameters(X9ObjectIdentifiers.Prime256v1, new SecureRandom());
       // static IdentityData data = Ts3Crypt.LoadIdentity("MG8DAgeAAgEgAiEAqNonGuL0w/8kLlgLbl4UkH8DQQJ7fEu1tLt+mx1E+XkCIQDgQoIGcBVvAvNoiDT37iWbPQb2kYe0+VKLg67OH2eQQwIgTyijCKx7QB/xQSnIW5uIkVmcm3UU5P2YnobR9IEEYPg=", 64, 0);
        //Notepad++ regex: .*\[(\d+)\]\: (\d+)\r\n
        static readonly IdentityData data = new IdentityData()
        {
            LastCheckedKeyOffset = 46075518,
            PrivateKey = new BigInteger(new byte[] { 0, 134, 149, 225, 225, 182, 233, 40, 11, 69, 105, 14, 139, 150, 189, 183, 116, 37, 137, 135, 186, 157, 216, 106, 27, 166, 213, 17, 131, 182, 113, 247, 5 }),
            PrivateKeyString = "MHADAgeAAgEgAiEAp8vOJXlGq2QVTdzrYdzxirdsD29C1PCTYRGkMPY2oL4CIQCkmQZuIPeWHkg7XOj48mAjOL9+seOkIvhStG3fxRU64wIhAIaV4eG26SgLRWkOi5a9t3QliYe6ndhqG6bVEYO2cfcF",
            PublicKey = keyGenParams.DomainParameters.Curve.CreatePoint(
                new BigInteger(new byte[] { 0, 167, 203, 206, 37, 121, 70, 171, 100, 21, 77, 220, 235, 97, 220, 241, 138, 183, 108, 15, 111, 66, 212, 240, 147, 97, 17, 164, 48, 246, 54, 160, 190 }),
                new BigInteger(new byte[] { 0, 164, 153, 6, 110, 32, 247, 150, 30, 72, 59, 92, 232, 248, 242, 96, 35, 56, 191, 126, 177, 227, 164, 34, 248, 82, 180, 109, 223, 197, 21, 58, 227 })),
            PublicKeyString = "ME0DAgcAAgEgAiEAp8vOJXlGq2QVTdzrYdzxirdsD29C1PCTYRGkMPY2oL4CIQCkmQZuIPeWHkg7XOj48mAjOL9+seOkIvhStG3fxRU64w==",
            ValidKeyOffset = 46075517
        };
        static readonly IdentityData data2 = new IdentityData()
        {
            LastCheckedKeyOffset = 22396876,
            PrivateKey = new BigInteger(new byte[] { 43, 168, 40, 197, 66, 169, 97, 155, 103, 27, 64, 7, 131, 137, 81, 100, 17, 198, 252, 239, 17, 115, 226, 255, 235, 7, 215, 219, 223, 163, 226, 30 }),
            PrivateKeyString = "MG4DAgeAAgEgAiA/i6yJQclHKR3qvB3uCtoWQ/qwqr5fADaB/8p06UuF0QIhAMU/ubQd5Kh65asaHK3VdQ+rTkoAMYTZkx/uV/QKxrgRAiArqCjFQqlhm2cbQAeDiVFkEcb87xFz4v/rB9fb36PiHg==",
            PublicKey = keyGenParams.DomainParameters.Curve.CreatePoint(
               new BigInteger(new byte[] { 63, 139, 172, 137, 65, 201, 71, 41, 29, 234, 188, 29, 238, 10, 218, 22, 67, 250, 176, 170, 190, 95, 0, 54, 129, 255, 202, 116, 233, 75, 133, 209 }),
               new BigInteger(new byte[] { 0, 197, 63, 185, 180, 29, 228, 168, 122, 229, 171, 26, 28, 173, 213, 117, 15, 171, 78, 74, 0, 49, 132, 217, 147, 31, 238, 87, 244, 10, 198, 184, 17 })),
            PublicKeyString = "MEwDAgcAAgEgAiA/i6yJQclHKR3qvB3uCtoWQ/qwqr5fADaB/8p06UuF0QIhAMU/ubQd5Kh65asaHK3VdQ+rTkoAMYTZkx/uV/QKxrgR",
            ValidKeyOffset = 22396875
        };
        //new ConnectionDataFull() { Address = "ts.dmg-inc.com", Username = "Kushzei_Bot", Identity = data, DefaultChannel = "/23209" };
        //static ConnectionDataFull connection = new ConnectionDataFull() { Address = "peruna.host", Username = "TestClient", Identity = data2, DefaultChannel = "/5" };
        ConnectionDataFull connection = null;

        TSBotInstanceConfig tsBotConfig = null;
        public Dictionary<string, TeamspeakClient> tsClients = new Dictionary<string, TeamspeakClient>();
        public Dictionary<string, TeamspeakGroup> tsGroups = new Dictionary<string, TeamspeakGroup>();
        Ts3FullClient client = null;
        //public EventHandler DoneLoading;
        public TeamspeakWrapper(TSBotInstanceConfig tsBotConfig)
        {
            this.tsBotConfig = tsBotConfig;
            //var data = createIdentity(Ts3Crypt.GenerateNewIdentity());
            //var dataTest = createIdentity(data2);
           // var dataaaa = compare(getIdentity(tsBotConfig.identity), data);
            connection = new ConnectionDataFull() {Address = tsBotConfig.ip, Username = tsBotConfig.username, Identity = getIdentity(tsBotConfig.identity), DefaultChannel = tsBotConfig.defaultChannel };
            Cmd.Execute += new EventHandler<MessageData>(handleCmd);
            SQLiteDB.get().
            PermissionManager.get().UserAdded += new UserEventHandler(onUserAdded);
            var dataaa = new BigInteger(new byte[] { 0x78, 30, 44, 206, 230, 122, 72, 75, 78, 226, 54, 0, 72, 114, 199, 162, 248, 248, 195, 131, 137, 223, 137, 42, 220, 111, 250, 156, 99, 194, 123, 7 });
            client = new Ts3FullClient(EventDispatchType.AutoThreadPooled);
            client.OnConnected += Client_OnConnected;
            client.OnDisconnected += Client_OnDisconnected;
            client.OnErrorEvent += Client_OnErrorEvent;
            client.OnTextMessageReceived += Client_OnTextMessageReceived;
            client.OnChannelList += Client_OnChannelList;
            client.Connect(connection);
        }
        public bool compare(IdentityData a, IdentityData b)
        {
            bool fake = false;
            if (a.ValidKeyOffset == b.ValidKeyOffset)
                fake = true;
            if (a.PrivateKeyString == b.PrivateKeyString)
                fake = true;
            if (a.PublicKeyString == b.PublicKeyString)
                fake = true;
            if (a.LastCheckedKeyOffset == b.LastCheckedKeyOffset)
                fake = true;
            if (a.PublicKey.XCoord.ToBigInteger() == b.PublicKey.XCoord.ToBigInteger())
                fake = true;
            if (a.PublicKey.YCoord.ToBigInteger() == b.PublicKey.YCoord.ToBigInteger())
                fake = true;
            return fake;
            /*
             *             ulong number = ulong.Parse(identity[0]);
        var privateInteger = deSerialize(identity[1]);
        string privateString = identity[2];
        var publicIntegerX = deSerialize(identity[3]);
        var publicIntegerY = deSerialize(identity[4]);
        string publicString = identity[5];
        ulong keyOffset = ulong.Parse(identity[6]);
             * */
        }

        public bool isConnected()
        {
            return client.Connected;
        }

        public void handleCmd(Object sender, MessageData msg)
        {
            if (!client.Connected)
                return;
            if (msg.args[0].ToLower().Equals("subchannel"))
            {
                createSubChannel(msg.args[1]);
                return;
            }
            else if (msg.args[0].ToLower().Equals("whoami"))
            {
                var data = client.WhoAmI();
                return;
            }
            else if (msg.args[0].ToLower().Equals("dcts"))
            {
                client.Disconnect();
                return;
            }
            else if (msg.args[0].ToLower().Equals("updatets"))
            {
                updateCurrentChannelName();
                updateCurrentChannelDescription();
                return;
            }
            else if (msg.args[0].ToLower().Equals("list"))
            {
                var dataTest = client.Send("channellist");
                foreach(var test in dataTest)
                {
                    Console.Out.WriteLine($"Server: {tsBotConfig.ip} Channel: {test["channel_name"]} id: {test["cid"]}");
                }
                return;
            }
        }

        public IdentityData getIdentity(string[] identity)
        {
            //string test = System.Text.Encoding.ASCII.GetString(System.Text.Encoding.ASCII.GetBytes("ÅåÄäÖö"));
            if(identity.Length != 7)
            {
                Console.Out.WriteLine("Invalid amount of identity strings!");
                return null;
            }
            ulong number = ulong.Parse(identity[0]);
            var privateInteger = deSerialize(identity[1]);
            string privateString = identity[2];
            var publicIntegerX = deSerialize(identity[3]);
            var publicIntegerY = deSerialize(identity[4]);
            string publicString = identity[5];
            ulong keyOffset = ulong.Parse(identity[6]);
            IdentityData data = new IdentityData()
            {
                LastCheckedKeyOffset = number,
                PrivateKey = privateInteger,
                PrivateKeyString = privateString,
                PublicKey = keyGenParams.DomainParameters.Curve.CreatePoint(publicIntegerX, publicIntegerY),
                PublicKeyString = publicString,
                ValidKeyOffset = keyOffset
            };
            return data;
        }

        string[] createIdentity(IdentityData identity)
        {
            List<string> identityData = new List<string>();
            identityData.Add(identity.LastCheckedKeyOffset.ToString());
            identityData.Add(serialize(identity.PrivateKey));
            identityData.Add(identity.PrivateKeyString);
            identityData.Add(serialize(identity.PublicKey.XCoord.ToBigInteger()));
            identityData.Add(serialize(identity.PublicKey.YCoord.ToBigInteger()));
            identityData.Add(identity.PublicKeyString);
            identityData.Add(identity.ValidKeyOffset.ToString());
            return identityData.ToArray();
        }

        public string serialize(BigInteger number)
        {
            return number.ToString();
        }

        public BigInteger deSerialize(string data)
        {
            return new BigInteger(data);
        }

        private void Client_OnDisconnected(object sender, DisconnectEventArgs e)
        {
            var client = (Ts3FullClient)sender;
            Console.WriteLine("Disconnected id {0}", client.ClientId);
            client.Connect(connection);
        }

        private void Client_OnChannelList(object sender, IEnumerable<ChannelList> e)
        {
            foreach (var msg in e)
            {
                if(msg.Name.ToLower().Contains("[bot]"))
                Console.Out.WriteLine($"{msg.Name} ID: {msg.ChannelId}");
            }
        }
        private void Client_OnConnected(object sender, EventArgs e)
        {
            var client = (Ts3FullClient)sender;
            Console.WriteLine("Connected id {0}", client.ClientId);
            var data = client.ClientInfo(client.ClientId);
            //client.Disconnect();
            //client.Connect(con);
        }

        private void Client_OnTextMessageReceived(object sender, IEnumerable<TextMessage> e)
        {
            foreach (var msg in e)
            {
                textMessageReceived(sender, msg);
            
            }
        }

        async Task textMessageReceived(object sender, TextMessage msg)
        {
            if (msg.Message == "Hi")
                Console.WriteLine("Hi" + msg.InvokerName);
            else if (msg.Message == "Exit")
            {
                var client = (Ts3FullClient)sender;
                var id = client.ClientId;
                Console.WriteLine("Exiting... {0}", id);
                client.Disconnect();
                Console.WriteLine("Exited... {0}", id);
            }
            else if (msg.Message == "upl")
            {
                var client = (Ts3FullClient)sender;
                FileInfo info  = new FileInfo("../CFG/nakkivene.png");
                var tok = client.FileTransferManager.UploadFile(info, 0, "/avatar", true);
                tok.Wait();
            }
            bool privateMessage = false;
            switch (msg.Target)
            {
                case TextMessageTargetMode.Channel:
                {
                    break;
                }
                case TextMessageTargetMode.Private:
                {
                    privateMessage = true;
                    break;
                }
                case TextMessageTargetMode.Server:
                {
                    break;
                }
            }

            var userMessage = Util.parseFromTeamspeak(msg.Message);
            if (userMessage.StartsWith("!"))
            {
                string[] splitData = userMessage.Substring(1).Split(' ');
                BotPlatform tsPlatform = new BotPlatform(this, BotPlatform.PlatformType.Teamspeak);
                NewTeamspeakMessageData tsMsgData = new NewTeamspeakMessageData();
                tsMsgData.platform = tsPlatform;
                tsMsgData.command = splitData[0];
                if (splitData.Length > 1)
                {
                    tsMsgData.args = new string[splitData.Length - 1];
                    Array.Copy(splitData, 1, tsMsgData.args, 0, splitData.Length - 1);
                }
                tsMsgData.privateMessage = privateMessage;
                tsMsgData.senderId = msg.InvokerId.ToString();
                tsMsgData.senderName = msg.InvokerName;
                tsMsgData.senderUid = msg.InvokerUid;

                CommandManager.get().handleCommand(tsMsgData);
            }
            else if (!userMessage.StartsWith("["))
            {
                UserData userData = PermissionManager.get().getUserByTsUid(msg.InvokerUid);
                if (userData == null)
                    return;
                if (userData.talkMode)
                {
                    if (privateMessage)
                        sendPrivateResponse(await CleverbotWrapper.getReponse(Util.parseFromTeamspeak(userMessage)), msg.InvokerId.ToString());
                    else
                        sendChannelResponse(await CleverbotWrapper.getReponse(Util.parseFromTeamspeak(userMessage)));
                }
            }
        }

        public void sendPrivateMessage(string message, string target)
        {
            client.Send("sendtextmessage",
            new CommandParameter("targetmode", 1),
            new CommandParameter("target", target),
            new CommandParameter("msg", message));
            //client.sendCommand($"sendtextmessage targetmode=1 target={target} msg={Util.parseToTeamspeak(message)}");
        }

        public void sendChannelMessage(string message)
        {
            client.Send("sendtextmessage",
            new CommandParameter("targetmode", 2),
            new CommandParameter("msg", message));
            //client.SendCommand($"sendtextmessage targetmode=2 msg={Util.parseToTeamspeak(message)}");
        }

        public void sendPrivateResponse(string message, string target)
        {
            sendPrivateMessage($"[BOT] {message}", target);
        }

        public void sendChannelResponse(string message)
        {
            sendChannelMessage($"[BOT] {message}");
        }

        public void createSubChannel(string channelName)
        {
            var data = client.Send("channelcreate",
                new CommandParameter("channel_name", channelName),
                new CommandParameter("channel_flag_temporary", 1),
                new CommandParameter("cpid", client.WhoAmI().ChannelId));
            Console.Out.WriteLine(data);
        }

        private void Client_OnErrorEvent(object sender, CommandError e)
        {
            var client = (Ts3FullClient)sender;
            Console.WriteLine(e.ErrorFormat());
            if(e.Id == Ts3ErrorCode.server_maxclients_reached)
            {
                Console.WriteLine("Max limit exceeded, reconnecting!");
                client.Connect(connection);
            }
            //if (!client.Connected)
            //{
            //	client.Connect(con);
            //}
        }
        Object lockObject = new object();


        public void onUserAdded(object sender, UserData userData)
        {
            //set default division
            //set default timezone (if applicable)
            if (tsClients.ContainsKey(userData.tsUserUid))
            {
                foreach (int number in tsClients[userData.tsUserUid].groups)
                {
                    if (tsGroups.ContainsKey(number.ToString()))
                    {
                        int divisionInt = Util.divisionStringToInt(tsGroups[number.ToString()].name);
                        if (divisionInt >= 0)
                            userData.division = (uint)divisionInt;
                    }
                }
                userData.countryCode = tsClients[userData.tsUserUid].countryCode;
                float timezone = 0;
                bool success = CountryTimeZones.get().getTimezone(tsClients[userData.tsUserUid].countryCode, out timezone);
                if (success)
                {
                    userData.timeZone = timezone;
                    userData.timezoneSet = true;
                }
            }
        }

        public bool setChannelDescription(string description)
        {
            try
            {
                if (!client.Connected)
                    return false;
                var whoAmI = client.WhoAmI();
                if (whoAmI == null)
                    return false;
                int channelId = (int)whoAmI.ChannelId;

                client.Send("channeledit",
            new CommandParameter("cid", channelId),
            new CommandParameter("channel_description", description));
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool setChannelName(string name)
        {
            try
            {
                if (!client.Connected)
                    return false;
                var whoAmI = client.WhoAmI();
                if (whoAmI == null)
                    return false;
                int channelId = (int)whoAmI.ChannelId;
                var channelInfo = client.Send("channelinfo",
                    new CommandParameter("cid", channelId));
                //Dont bother replacing if name already is the same
                string nameParsed = name.Length > 40 ? name.Substring(0, 40) : name;
                foreach (var x in channelInfo)
                {
                    string testData = nameParsed.Replace(" ", "\\s");
                    if (x["channel_name"] == testData)
                        return true;
                }
                client.Send("channeledit",
                new CommandParameter("cid", channelId),
                new CommandParameter("channel_name", nameParsed));
                return true;
            }
            catch(Exception ex)

            {
                return false;
            }
        }

        public void update()
        {
            if (canUpdateCurrentChannelName())
                updateCurrentChannelName();
            if (canUpdateChannelDescription())
                updateCurrentChannelDescription();
        }

        bool canUpdateChannelDescription()
        {
            if ((DateTime.UtcNow - latestChannelDescUpdateTime).TotalMinutes < tsBotConfig.channelDescUpdateRateMinutes)
                return false;
            return true;
        }

        public void updateCurrentChannelDescription()
        {
            try
            {
                var events = getCurrentChannelEvents();
                string eventString = "";
                eventString += "All times are in UTC+0\n";
                eventString += "[b]Next events:[/b]";
                eventString += "[list]";
                foreach (Event e in events)
                {
                    // if(Event.)
                    eventString += $"[*]{e.toDescriptionString()}\n";
                }
                eventString += "[/list]";
                string updatedString = $"\nLast update was at {DateTime.UtcNow.ToString("dd/MM/yyyy hh:mm tt")}";
                eventString += updatedString;

                if(setChannelDescription(eventString))
                    latestChannelDescUpdateTime = DateTime.UtcNow;
                Util.log($"Current Memory Usage: {(GC.GetTotalMemory(false) / 1024.0 / 1024.0)}");
            }
            catch (Exception ex)
            {
                Util.log(ex.ToString());
            }
        }

        bool canUpdateCurrentChannelName()
        {
            if ((DateTime.UtcNow - latestChannelNameUpdateTime).TotalMinutes < tsBotConfig.channelNameUpdateRateMinutes)
                return false;
            return true;
        }

        public void updateCurrentChannelName()
        {
            string eventString = "[Bot]";
            var events = getCurrentChannelEvents();
            if (events.Length > 0)
            {
                DateTime firstEventStartTimeLocal = events[0].startDate;
                TimeSpan firstTimeDifference = firstEventStartTimeLocal - DateTime.UtcNow;
                if (firstTimeDifference.TotalMinutes > 61)
                {
                    eventString = $"{events[0].title} in {Math.Ceiling(firstTimeDifference.TotalHours)} hours";
                }
                else
                {
                    List<Event> closeEvents = new List<Event>();
                    foreach (Event e in events)
                    {
                        DateTime eventStartTimeLocal = e.startDate;
                        TimeSpan timeDifference = eventStartTimeLocal - DateTime.UtcNow;
                        if (timeDifference.TotalMinutes < 61)
                            closeEvents.Add(e);
                    }
                    if (closeEvents.Count == 1)
                    {
                        DateTime eventStartTimeLocal = closeEvents[0].startDate;
                        TimeSpan timeDifference = eventStartTimeLocal - DateTime.UtcNow;
                        eventString = $"{events[0].title} in {Math.Ceiling(timeDifference.TotalMinutes)} mins";
                    }
                    else
                    {
                        DateTime eventStartTimeLocal = closeEvents[0].startDate;
                        TimeSpan timeDifference = eventStartTimeLocal - DateTime.UtcNow;
                        eventString = $"{closeEvents.Count} events, {Math.Ceiling(timeDifference.TotalMinutes)}mins {events[0].title}";
                    }
                }
            }
            else
            {
                eventString = $"no events start anytime soon";
            }
            if(setChannelName(eventString))
                latestChannelNameUpdateTime = DateTime.UtcNow;
        }

        Common.EventFetcher.Event[] getCurrentChannelEvents()
        {
            List<uint> divNumbers = new List<uint>();
            foreach (string div in tsBotConfig.divisions)
            {
                divNumbers.Add(uint.Parse(div));
            }
            return EventFetcher.get().getDivisionEvents(divNumbers.ToArray());
        }
    }
}


