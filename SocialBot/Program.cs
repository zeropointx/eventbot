﻿using Common;
using CommonLibrary.Database;
using DiscordBot;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Teamspeak.NewClient;
using TS3Client;
using TS3Client.Full;
using TS3Client.Messages;
//using TS3ClientQuery;
namespace Start
{
    public static class Program
    {
        static ConnectionDataFull con;
        static List<Thread> teamspeakThreads = new List<Thread>();
        static List<TeamspeakWrapper> teamspeaks = new List<TeamspeakWrapper>();
        static int number = 0;
        //public static DiscordTest2 discord = null;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static void Main(string[] args)
        {
            Common.Config.ConfigManager.initialize();
            SQLiteDB.get().connect();
            //var events = SQLiteDB.get().getEvents(new SQLiteDB.DBParam(SQLiteDB.DBParamType.DIVISION, 15));
            //System.Diagnostics.Debugger.Launch();
            Common.Util.log("Starting cleverbot");
            Common.CleverbotWrapper.init();
            Thread updateThread = new Thread(update);
            updateThread.Start();

            if ((bool)Common.Config.ConfigManager.getGlobal().getConfig("discordEnabled"))
            {
                Common.Util.log("Starting Discord");
                DiscordTest2.get().createThreaded();
                AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnProcessExitDiscord);
            }
            if ((bool)Common.Config.ConfigManager.getGlobal().getConfig("teamspeakEnabled"))
            {
                foreach(var botConfig in Common.Config.ConfigManager.get().tsbotInstanceConfigs)
                {
                    
                    Thread thread = new Thread(() =>
                    {
                        Thread.Sleep(Program.number++ * 1000);
                        TeamspeakWrapper newTeamspeak = new TeamspeakWrapper(botConfig.Value);
                        teamspeaks.Add(newTeamspeak);
                        while(true)
                        {
                            Thread.Sleep(1000);
                            newTeamspeak.update();
                        }
                    });
                    thread.Start();
                    teamspeakThreads.Add(thread);
                }
                
                AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnProcessExitTS);
                /*
                var clients = new List<Ts3FullClient>();

                for (int i = 0; i < 1; i++)
                {
                    var client = new Ts3FullClient(EventDispatchType.AutoThreadPooled);
                    client.OnConnected += Client_OnConnected;
                    client.OnDisconnected += Client_OnDisconnected;
                    client.OnErrorEvent += Client_OnErrorEvent;
                    client.OnTextMessageReceived += Client_OnTextMessageReceived;
                    var data = Ts3Crypt.LoadIdentity("MG8DAgeAAgEgAiEAqNonGuL0w/8kLlgLbl4UkH8DQQJ7fEu1tLt+mx1E+XkCIQDgQoIGcBVvAvNoiDT37iWbPQb2kYe0+VKLg67OH2eQQwIgTyijCKx7QB/xQSnIW5uIkVmcm3UU5P2YnobR9IEEYPg=", 64, 0);
                    con = new ConnectionDataFull() { Address = "peruna.host", Username = "TestClient", Identity = data };
                    client.Connect(con);
                    clients.Add(client);
                }*/
            }
            updateThread.Join();
        }
        static void OnProcessExitTS(object sender, EventArgs e)
        {
            //if (newTeamspeak.isConnected())
            //    newTeamspeak.setChannelName("Bot is down");
        }
        static void OnProcessExitDiscord(object sender, EventArgs e)
        {
        }
        static void update()
        {
            updateAsync();
            //Thread.Sleep(100000);
        }
        static async void updateAsync()
        {
            Thread.Sleep(2000);
            while (true)
            {
                await EventScheduler.get().update();
                Thread.Sleep(500);
            }
        }
        /*
        private static void Client_OnDisconnected(object sender, DisconnectEventArgs e)
        {
            var client = (Ts3FullClient)sender;
            Console.WriteLine("Disconnected id {0}", client.ClientId);
        }

        private static void Client_OnConnected(object sender, EventArgs e)
        {
            var client = (Ts3FullClient)sender;
            Console.WriteLine("Connected id {0}", client.ClientId);
            var data = client.ClientInfo(client.ClientId);
            //client.Disconnect();
            //client.Connect(con);
        }

        private static void Client_OnTextMessageReceived(object sender, IEnumerable<TextMessage> e)
        {
            foreach (var msg in e)
            {
                if (msg.Message == "Hi")
                    Console.WriteLine("Hi" + msg.InvokerName);
                else if (msg.Message == "Exit")
                {
                    var client = (Ts3FullClient)sender;
                    var id = client.ClientId;
                    Console.WriteLine("Exiting... {0}", id);
                    client.Disconnect();
                    Console.WriteLine("Exited... {0}", id);
                }
                else if (msg.Message == "upl")
                {
                    var client = (Ts3FullClient)sender;

                    var tok = client.FileTransferManager.UploadFile(new FileInfo("img.png"), 0, "/avatar", true);
                    tok.Wait();
                }
            }
        }

        private static void Client_OnErrorEvent(object sender, CommandError e)
        {
            //var client = (Ts3FullClient)sender;
            //Console.WriteLine(e.ErrorFormat());
            //if (!client.Connected)
            //{
            //	client.Connect(con);
            //}
        }*/
    }
}
