﻿using Common;
using Common.Config;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using static Common.CommandManager;
using static Common.EventFetcher;
using static Common.PermissionManager;

namespace DiscordBot
{
    public class DiscordTest2
    {
        public DiscordClient client = null;

        static DiscordTest2 discordTest = new DiscordTest2();
        public static DiscordTest2 get() { return discordTest; }
        DateTime latestChannelNameUpdateTime = new DateTime(0);

        public class DiscordMessageData : MessageData
        {
            public override PermissionManager.UserData getUserData()
            {
                UserData userData = null;
                userData = Common.PermissionManager.get().getUserByDID(senderId);
                return userData;
            }

            public async override Task sendChannelResponse(string message)
            {
                if (platform.platformType != BotPlatform.PlatformType.Discord)
                    return;
                MessageCreateEventArgs msgEvent = (MessageCreateEventArgs)platform.platform;
                await msgEvent.Message.Channel.SendMessageAsync(message);
            }

            public async override Task sendPrivateResponse(string message)
            {
                if (platform.platformType != BotPlatform.PlatformType.Discord)
                    return;
                MessageCreateEventArgs msgEvent = (MessageCreateEventArgs)platform.platform;
                if (msgEvent == null)
                    return;
                DiscordMember member = await get().getMember(msgEvent.Author.Id);
                if(member != null)
                await member.SendMessageAsync(message);
            }
        }
        public DiscordTest2()
        {
            Cmd.Execute += new EventHandler<MessageData>(handleCmd);
            Cat.Execute += new EventHandler<MessageData>(handleCat);
        }


        public void createThreaded()
        {
            Thread t = new Thread(update);
            t.Start();
        }
        public async Task<DiscordMember> getMember(ulong id)
        {
            ulong guildId = (ulong)(long)ConfigManager.getGlobal().getConfig("discordGuild");
            DiscordGuild guild = await client.GetGuildAsync(guildId);
            if (guild == null)
                return null;
            DiscordMember member = await guild.GetMemberAsync(id);
            return member;
        }
        public void update()
        {
            client = new DiscordClient(new DiscordConfiguration
            {
                Token = (string)ConfigManager.getGlobal().getConfig("discordToken"),
                TokenType = TokenType.Bot
            });
            //client.SetWebSocketClient<WebSocketSharpClient>();
            client.MessageCreated += new AsyncEventHandler<MessageCreateEventArgs>(handleMessage);
            client.ConnectAsync();
            while(true)
            {
                Thread.Sleep(1000);
                if (canUpdateCurrentChannelName())
                    updateCurrentChannelName();
            }
        }
        public async void handleCat(Object sender, MessageData message)
        {
            int attemps = 0;
            while (attemps <= 5)
            {
                try
                {
                    attemps++;
                    HttpClient httpClient = new HttpClient() { MaxResponseContentBufferSize = 1000000 };
                    HttpResponseMessage data = await httpClient.GetAsync("http://thecatapi.com/api/images/get");
                    Stream stream = await data.Content.ReadAsStreamAsync();
                    MessageCreateEventArgs e = (MessageCreateEventArgs)message.platform.platform;
                    string type = data.Content.Headers.ContentType.MediaType;
                    string parsedType = type.Substring(type.IndexOf("/") + 1);
                    await e.Channel.SendFileAsync(stream, $"data.{parsedType}");
                    break;
                }
                catch (Exception e)
                {
                    Util.log(e.ToString());
                }
            }
        }
        public async void handleCmd(Object sender, MessageData msg)
        {
            if (msg.args[0].ToLower().Equals("dcds"))
            {
                await client.DisconnectAsync();
                return;
            }
            else if (msg.args[0].ToLower().Equals("updateds"))
            {
                updateCurrentChannelName();
                return;
            }
        }
        public async Task handleMessage( MessageCreateEventArgs e)
        {
            try
            {
                //foreach(var guild in client.Guilds)
                //{
                //    Console.Out.WriteLine(guild);
                //}
                if (e.Channel.Name != null && e.Channel.Name.ToLower() != (string)ConfigManager.getGlobal().getConfig("discordChannel"))
                    return;
                if (e.Author.Id == client.CurrentUser.Id)
                    return;
                if (e.Message.Content.StartsWith("!"))
                {
                    string[] splitData = e.Message.Content.Substring(1).Split(' ');
                    BotPlatform discordPlatform = new BotPlatform(e, BotPlatform.PlatformType.Discord);
                    DiscordMessageData disMsgData = new DiscordMessageData();
                    disMsgData.platform = discordPlatform;
                    disMsgData.command = splitData[0];
                    if (splitData.Length > 1)
                    {
                        disMsgData.args = new string[splitData.Length - 1];
                        Array.Copy(splitData, 1, disMsgData.args, 0, splitData.Length - 1);
                    }
                    disMsgData.privateMessage = e.Channel.Name == null;
                    disMsgData.senderId = e.Author.Id.ToString();
                    disMsgData.senderName = e.Author.Username;
                    disMsgData.senderUid = "";
                    CommandManager.get().handleCommand(disMsgData);
                }
                else
                {
                    UserData userData = null;
                    userData = Common.PermissionManager.get().getUserByDID(e.Author.Id.ToString());
                    if (userData != null)
                    {
                        if (userData.talkMode)
                            await e.Channel.SendMessageAsync(await CleverbotWrapper.getReponse(e.Message.Content));
                    }
                }
                
                /*
                if (e.Message.Content.ToLower().Equals("!ping"))
                {
                    DiscordEmbedBuilder builder = new DiscordEmbedBuilder();
                    builder.WithTitle("pong").WithImageUrl("http://a.abcnews.com/images/Health/abc_pingpong_trailer_130529_wg.jpg");
                    await e.Message.RespondAsync(null, false, builder.Build());
                }
                else if (e.Message.Content == "!nextevent")
                {
                    Event nextEvent = EventFetcher.get().getNextEvent(15);
                    if (nextEvent != null)
                        await e.Message.Channel.SendMessageAsync(nextEvent.toString());
                    else
                        await e.Message.Channel.SendMessageAsync("Bot can't find any events");
                }
                else if (e.Message.Content.StartsWith("!btag", StringComparison.InvariantCultureIgnoreCase))
                {

                    //string baseUrl = "https://playoverwatch.com/en-us/career/pc/eu/";
                    string baseUrl = "https://masteroverwatch.com/profile/pc/eu/";
                    string secondPart = e.Message.Content.Split(' ')[1].Replace('#', '-');
                    string finalUrl = $"{baseUrl}{secondPart}";
                    string testurl = $"https://ow-api.com/v1/stats/pc/eu/{secondPart}/profile";

                    HttpClient httpClient = new HttpClient() { MaxResponseContentBufferSize = 1000000 };
                    string data = await httpClient.GetStringAsync(testurl);
                    JsonTextReader reader = new JsonTextReader(new StringReader(data));
                    JObject o = (JObject)JToken.ReadFrom(reader);
                    string rating = "";
                    string ratingIcon = "";
                    foreach (JToken token in o.Children())
                    {
                        JProperty property = (JProperty)token;
                        if (property.Name == "rating")
                            rating = (string)property.Value;
                        else if (property.Name == "ratingIcon")
                            ratingIcon = (string)property.Value;
                    }
                    //EmbedBuilder builder = new EmbedBuilder();
                    //builder.WithUrl(finalUrl);
                    //await message.Channel.SendMessageAsync("",false, builder.Build());
                    DiscordEmbedBuilder builder = new DiscordEmbedBuilder();
                    builder.WithTitle($"Rating {rating}").WithImageUrl(ratingIcon).WithUrl(finalUrl);
                    await e.Message.Channel.SendMessageAsync(null, false, builder.Build());
                    //await message.Channel.SendMessageAsync($"{baseUrl2}{secondPart.Replace('#', '-')}");
                }
                else
                {
                    await e.Message.Channel.SendMessageAsync(await CleverbotWrapper.getReponse(e.Message.Content));
                }*/
            }
            catch(Exception ex)
            {
                await e.Message.Channel.SendMessageAsync(ex.ToString());
            }
        }

        bool canUpdateCurrentChannelName()
        {
            if ((DateTime.UtcNow - latestChannelNameUpdateTime).TotalMinutes < Convert.ToUInt32(ConfigManager.getGlobal().getConfig("discordUpdateChannelNameRateMinutes")))
                return false;
            return true;
        }

        public void updateCurrentChannelName()
        {
            string eventString = "";
            var events = getCurrentChannelEvents();
            if (events.Length > 0)
            {
                DateTime firstEventStartTimeLocal = events[0].startDate;
                TimeSpan firstTimeDifference = firstEventStartTimeLocal - DateTime.UtcNow;
                if (firstTimeDifference.TotalMinutes > 61)
                {
                    eventString = $"{events[0].title} in {Math.Ceiling(firstTimeDifference.TotalHours)} hours";
                }
                else
                {
                    List<Event> closeEvents = new List<Event>();
                    foreach (Event e in events)
                    {
                        DateTime eventStartTimeLocal = e.startDate;
                        TimeSpan timeDifference = eventStartTimeLocal - DateTime.UtcNow;
                        if (timeDifference.TotalMinutes < 61)
                            closeEvents.Add(e);
                    }
                    if (closeEvents.Count == 1)
                    {
                        DateTime eventStartTimeLocal = closeEvents[0].startDate;
                        TimeSpan timeDifference = eventStartTimeLocal - DateTime.UtcNow;
                        eventString = $"{events[0].title} in {Math.Ceiling(timeDifference.TotalMinutes)} mins";
                    }
                    else
                    {
                        DateTime eventStartTimeLocal = closeEvents[0].startDate;
                        TimeSpan timeDifference = eventStartTimeLocal - DateTime.UtcNow;
                        eventString = $"{closeEvents.Count} events, {Math.Ceiling(timeDifference.TotalMinutes)}mins {events[0].title}";
                    }
                }
            }
            else
            {
                eventString = $"no events start anytime soon";
            }
          
            if (client == null || client.CurrentApplication == null)
            {
                return;
            }
            DSharpPlus.Entities.DiscordGame discordGame = new DiscordGame(eventString);
            client.UpdateStatusAsync(game: discordGame);
            latestChannelNameUpdateTime = DateTime.UtcNow;
        }

        Common.EventFetcher.Event[] getCurrentChannelEvents()
        {
            string divisions = (string)ConfigManager.getGlobal().getConfig("discordCurrentChannelDivisions");
            string[] divisionsSplit = divisions.Split(',');
            List<uint> divNumbers = new List<uint>();
            foreach (string div in divisionsSplit)
            {
                divNumbers.Add(uint.Parse(div));
            }
            return EventFetcher.get().getDivisionEvents(divNumbers.ToArray());
        }
    }
}
