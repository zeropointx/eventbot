﻿using Common;
using Common.Config;
using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using static Common.EventFetcher;
namespace CommonLibrary.Database
{
    public class SQLiteDB
    {
        static SQLiteDB db = new SQLiteDB();
        public static SQLiteDB get() { return db; }
        static public EventHandler<Event[]> UpdateSignal;
        public enum DBParamType
        {
            [Description("id")]
            ID ,
            [Description("hostingOfficer")]
            HOSTING_OFFICER,
            [Description("title")]
            TITLE,
            [Description("rawTitle")]
            RAW_TITLE,
            [Description("division")]
            DIVISION,
            [Description("game")]
            GAME,
            [Description("startTime")]
            START_TIME,
            [Description("endTime")]
            END_TIME,
            [Description("rawData")]
            RAW_DATA,
            [Description("attendingMembersRawData")]
            ATTENDING_MEMBERS
        }
        public enum DBOperator
        {
            [Description(">")]
            GREATER_THAN,
            [Description("=")]
            EQUALS,
            [Description("<")]
            SMALLER_THAN,
            [Description(">=")]
            GREATER_EQUAL,
            [Description("<=")]
            SMALLER_EQUAL,
            [Description("LIKE")]
            LIKE,
        }
        public struct DBParam
        {
            public DBParam(DBParamType paramType, object[] values, DBOperator dbOperator)
            {
                this.paramType = paramType;
                this.values = values;
                this.dbOperator = dbOperator;
            }
            public DBParamType paramType;
            public DBOperator dbOperator;
            public object[] values;
        }

        SqliteConnection dbConnection = null;
        public SQLiteDB()
        {
            string fileName = (string)ConfigManager.getGlobal().getConfig("sqliteFile");
            DirectoryInfo dir = new DirectoryInfo($"{fileName}");
            Util.logAndPrint($"sqlite dir {dir.FullName}");
            dbConnection = new SqliteConnection("" +
            new SqliteConnectionStringBuilder
            {
                DataSource = $"{dir.FullName}"
            });
        }

        public void connect()
        {
            dbConnection.Open();
            createTableIfNotExists();
        }

        public void createTableIfNotExists()
        {
            runQuery("CREATE TABLE IF NOT EXISTS events (id integer UNIQUE, hostingOfficer text NOT NULL, title text NOT NULL, rawTitle text NOT NULL, division integer, game text NOT NULL, " +
                "startTime TIMESTAMP, endTime TIMESTAMP, rawData text, attendingMembersRawData text, updateTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP, creationTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY(id));");
        }

        public void insertEvent(Event e)
        {
            insertEvent(e.id, e.hostingOfficer, e.title, e.rawTitle, e.division, e.game, e.startDate, e.endDate, e.rawData, e.attendingMembersRawData);
        }

        public void insertEvent(int id, string hostingOfficer, string title, string rawTitle, int division, string game, DateTime startTime, DateTime endTime, string rawData, string attendingMembersRawData)
        {
            if(getEvent(id) == null)
            {
                runQuery("INSERT INTO events (id, hostingOfficer, title, rawTitle, division, game, startTime, endTime, rawData, attendingMembersRawData) VALUES(@par0, @par1, @par2, @par3, @par4, @par5, @par6, @par7, @par8, @par9)",
                   id, hostingOfficer, title, rawTitle, division, game, startTime, endTime, rawData, attendingMembersRawData);
            }
        }

        public void updateEvent(Event e)
        {
            updateEvent(e.id, e.hostingOfficer, e.title, e.rawTitle, e.division, e.game, e.startDate, e.endDate, e.rawData, e.attendingMembersRawData);
        }

        public void updateEvent(int id, string hostingOfficer, string title, string rawTitle, int division, string game, DateTime startTime, DateTime endTime, string rawData, string attendingMembersRawData)
        {
            Event oldEvent = getEvent(id);
            if (oldEvent != null)
            {
                oldEvent = new Event(oldEvent);
                runQuery("UPDATE events SET hostingOfficer = @par0, title = @par1, rawTitle = @par2, division = @par3, game = @par4, startTime = @par5, endTime = @par6, rawData = @par7, attendingMembersRawData = @par8, updateTime = @par9 WHERE id = @par10",
                    hostingOfficer, title, rawTitle, division, game, startTime, endTime, rawData, attendingMembersRawData, DateTime.UtcNow, id);
                Event newEvent = getEvent(id);
                UpdateSignal?.Invoke(this, new Event[] { oldEvent, newEvent });
            }
        }

        public Event getEvent(int id)
        {
            var events = runQueryWithReturn("Select * from events WHERE id = @par0 LIMIT 1", id);
            if (events.Length != 1)
                return null;
            return events[0];
        }

        public Event[] getEvents(params DBParam[] parameters)
        {
            string queryString = "Select * from events";
            Event[] events = null;
            if (parameters == null)
            {
                
                events = runQueryWithReturn($"{queryString} ORDER BY startTime ASC");
            }
            else
            {
                if(parameters != null && parameters.Length != 0)
                queryString += " where ";
                List<Object> objects = new List<Object>();
                uint index = 0;
                for(int i = 0; i < parameters.Length; i++)
                {
                    if (parameters[i].values == null || parameters[i].values.Length == 0)
                    {
                        return null;
                    }
                    else if (parameters[i].values.Length == 1)
                    {
                        queryString += $" {parameters[i].paramType.ToDescriptionString()} {parameters[i].dbOperator.ToDescriptionString()} @par{index}";
                        if (i != parameters.Length - 1)
                            queryString += " AND ";
                        objects.Add(parameters[i].values[0]);
                        index++;
                    }
                    else
                    {
                        queryString += " (";
                        for (int j = 0; j < parameters[i].values.Length; j++)
                        {
                            
                            queryString += $"{parameters[i].paramType.ToDescriptionString()} {parameters[i].dbOperator.ToDescriptionString()} @par{index}";
                            if(j != parameters[i].values.Length -1)
                            {
                                queryString += " OR ";
                            }
                            objects.Add(parameters[i].values[j]);
                            index++;
                        }
                        queryString += ")";
                        if (i != parameters.Length - 1)
                            queryString += " AND ";
                    }
                }
                events = runQueryWithReturn($"{queryString} ORDER BY startTime ASC", objects.ToArray());
            }
            return events;
        }
        void runQuery(string queryString, params object[] values)
        {
            using (var transaction = dbConnection.BeginTransaction())
            {
                var command = dbConnection.CreateCommand();
                command.Transaction = transaction;
                command.CommandText = queryString;
                if (values != null)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        command.Parameters.AddWithValue($"@par{i}", values[i]);
                    }
                }
                command.ExecuteNonQuery();
                transaction.Commit();
            }

        }
        Event[] runQueryWithReturn(string queryString, params object[] values)
        {
            List<Event> events = new List<Event>();

            using (var transaction = dbConnection.BeginTransaction())
            {
                var command = dbConnection.CreateCommand();
                command.Transaction = transaction;
                command.CommandText = queryString;
                if (values != null)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        command.Parameters.AddWithValue($"@par{i}", values[i]);
                    }
                }
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Event e = new Event();
                        e.id = (int)(Int64)reader["id"];
                        e.hostingOfficer = (string)reader["hostingOfficer"];
                        e.title = (string)reader["title"];
                        e.rawTitle = (string)reader["rawTitle"];
                        e.division = (int)(Int64)reader["division"];
                        e.game = (string)reader["game"];
                        var startDateString = (string)reader["startTime"];
                        e.startDate = DateTime.Parse(startDateString);
                        var endDateString = (string)reader["endTime"];
                        e.endDate = DateTime.Parse(endDateString);
                        e.rawData = (string)reader["rawData"];
                        e.setAttendingMembers((string)reader["attendingMembersRawData"]);
                        events.Add(e);
                    }
                }
                //transaction.Commit();
            }
            return events.ToArray();
        }
    }
}
