﻿using Common.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static CommonLibrary.Database.SQLiteDB;

namespace Common
{
    public static class Util
    {
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp);
            return dtDateTime;
        }
        public static long ToUnixTime(this DateTime date)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return Convert.ToInt64((date.ToUniversalTime() - epoch).TotalSeconds);
        }
        public static string parseToTeamspeak(string message)
        {
            return message.Replace(":0", ";0");
        }
        public static string parseFromTeamspeak(string message)
        {
            return message.Replace("\\s", " ");
        }
        public static int parseDivision(string parameter)
        {
            int number;
            bool canParseNumber = int.TryParse(parameter, out number);
            if (canParseNumber)
                return number;
            else if (parameter.ToLower() == "clan")
                return 0;
            string start = "di-";
            for (int i = 1; i <120; i++)
            {
                bool match = parameter.ToLower().Equals(start + ToRoman(i).ToLower()) || parameter.ToLower().Equals(ToRoman(i).ToLower());
                if (match)
                    return i;
            }
            return -1;
        }
        public static int divisionStringToInt(string division)
        {
            return parseDivision(division);
        }
        public static string divisionToString(int number)
        {
            if (number == -1)
                return "Unknown";
            if (number == 0)
                return "clan";
            if (number > 0 && number < 3999)
                return $"DI-{ToRoman((int)number)}";
            else
                return number.ToString();
        }
        public static DateTime toCurrentDate(DateTime utc0Time)
        {
            return utc0Time + TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);
        }
        public static bool getTimeInfo(string countryCode, out float timeZone)
        {
            var cultures = CultureInfo.GetCultures(CultureTypes.AllCultures).Where(c => !c.IsNeutralCulture);
            var cultureInfos = CultureInfo.GetCultures(CultureTypes.AllCultures)
                              .Where(c => c.Name.ToLower().Equals($"{countryCode.ToLower()}-{countryCode.ToLower()}", StringComparison.InvariantCultureIgnoreCase) && !c.IsNeutralCulture);
            List<TimeZoneInfo> tzList = TimeZoneInfo.GetSystemTimeZones().ToList();
            foreach (var culture in cultureInfos)
            {
                if (culture.Name.Equals(culture.TwoLetterISOLanguageName.ToLower() + "-" + culture.TwoLetterISOLanguageName.ToUpper(), StringComparison.InvariantCultureIgnoreCase))
                {
                    Util.log(culture.TwoLetterISOLanguageName);
                    var regions = CultureInfo.GetCultures(CultureTypes.SpecificCultures).Select(x => new RegionInfo(culture.LCID));
                    var region = regions.First();
                    int found = 0;
                    TimeZoneInfo latestTimezone = null;
                    foreach (TimeZoneInfo tzInfo in tzList)
                    {
                        if (tzInfo.Id.ToLower().Contains(region.EnglishName.ToLower()))
                        {
                            latestTimezone = tzInfo;
                            found++;
                        }
                    }
                    if (found == 1)
                    {
                        timeZone = latestTimezone.BaseUtcOffset.Minutes / 60;
                        return true;
                    }
                }
            }
            timeZone = 0;
            return false;
        }
        public static string timezoneString(float timezone)
        {
            string sign = timezone > 0 ? "+" : "";
            return $"UTC{sign}{timezone}";
        }
        public static string ToRoman(int number)
        {
            if ((number < 0) || (number > 3999)) throw new ArgumentOutOfRangeException("insert value betwheen 1 and 3999");
            if (number < 1) return string.Empty;
            if (number >= 1000) return "M" + ToRoman(number - 1000);
            if (number >= 900) return "CM" + ToRoman(number - 900); //EDIT: i've typed 400 instead 900
            if (number >= 500) return "D" + ToRoman(number - 500);
            if (number >= 400) return "CD" + ToRoman(number - 400);
            if (number >= 100) return "C" + ToRoman(number - 100);
            if (number >= 90) return "XC" + ToRoman(number - 90);
            if (number >= 50) return "L" + ToRoman(number - 50);
            if (number >= 40) return "XL" + ToRoman(number - 40);
            if (number >= 10) return "X" + ToRoman(number - 10);
            if (number >= 9) return "IX" + ToRoman(number - 9);
            if (number >= 5) return "V" + ToRoman(number - 5);
            if (number >= 4) return "IV" + ToRoman(number - 4);
            if (number >= 1) return "I" + ToRoman(number - 1);
            return "";
        }
        static string path = "";
        static string logFileName = "log";
        static string logFileExtension = ".txt";
        static uint maxSize = 10000000;
        private static Mutex mut = new Mutex();
        static PrintLevel printLevel = PrintLevel.DEBUG;
        public enum PrintLevel
        {
            DEBUG,
            NORMAL,
            NONE
        }
        public static void log(string message, PrintLevel printLevel = PrintLevel.NORMAL)
        {
            uint logFileCount = 1;
            /*
             * Find logfile path. If log.txt is used and it has more than maxSize characters create new.
             */
            while (path == "")
            {
                string tempPath = "";
                if (path == "" && logFileCount == 1)
                    tempPath = $"{ConfigManager.getGlobal().getConfig("logFolder")}{logFileName}{logFileExtension}";
                else
                    tempPath = $"{ConfigManager.getGlobal().getConfig("logFolder")}{logFileName}{logFileCount}{logFileExtension}";
                bool fileExists = File.Exists(tempPath);
                if (!fileExists)
                {
                    FileStream stream = File.Create(tempPath);
                    stream.Close();
                    path = tempPath;
                    break;
                }
                else
                {
                    FileInfo f = new FileInfo(tempPath);
                    if(f.Length < maxSize)
                    {
                        path = tempPath;
                        break;
                    }
                }
                logFileCount++;
            }

            if(mut.WaitOne(5000) && Util.printLevel <= printLevel)
            {
                StreamWriter strmWriter = new StreamWriter(path, true);
                strmWriter.WriteLine($"{DateTime.Now} {message}");
                strmWriter.Close();
                mut.ReleaseMutex();
            }
        }
        public static void logAndPrint(string message, PrintLevel printLevel = PrintLevel.NORMAL)
        {
            if (Util.printLevel <= printLevel)
            {
                Console.Out.WriteLine(message);
                log(message);
            }
        }
        public static string ToDescriptionString(this DBParamType val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val.GetType().GetField(val.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
        public static string ToDescriptionString(this DBOperator val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val.GetType().GetField(val.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }
}
