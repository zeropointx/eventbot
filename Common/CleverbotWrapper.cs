﻿using Common.Config;
using HtmlAgilityPack;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;


namespace Common
{
    public class CleverbotWrapper
    {
        static string conversation = "";
        static bool debugResponse = false;
        public CleverbotWrapper()
        {
            
        }
        public static async void init()
        {
            await initTest();
        }
        public static async Task initTest()
        {
            string initUrl = $"https://www.cleverbot.com/getreply?key=" + (string)ConfigManager.getGlobal().getConfig("cleverbotApikey");
            HttpClient client = new HttpClient() { MaxResponseContentBufferSize = 1000000 };
            string data = await client.GetStringAsync(initUrl);
            
            JsonTextReader reader = new JsonTextReader(new StringReader(data));
            while (reader.Read())
            {
                if (reader.Value != null)
                {
                    if (reader.Value.Equals("cs"))
                    {
                        reader.Read();
                        if (reader.Value == null)
                            continue;
                        conversation = (string)reader.Value;
                    }
                    if (debugResponse)
                    Console.WriteLine("Token: {0}, Value: {1}", reader.TokenType, reader.Value);
                }
                else
                {
                    if(debugResponse)
                        Console.WriteLine("Token: {0}", reader.TokenType);
                }
            }
        }
        public static async Task<string> getReponse(string message)
        {
            return await test(message);
        }
        static string baseResponseUrl = "https://www.cleverbot.com/getreply";
        public static async Task<string> test(string message)
        {

            HttpClient client = new HttpClient() { MaxResponseContentBufferSize = 1000000 };
            string data = await client.GetStringAsync($"{baseResponseUrl}?key={(string)ConfigManager.getGlobal().getConfig("cleverbotApikey")}&input={message}&cs={conversation}");
            JsonTextReader reader = new JsonTextReader(new StringReader(data));
            while (reader.Read())
            {
                if (reader.Value != null)
                {
                    if (reader.Value.Equals("cs"))
                    {
                        reader.Read();
                        if (reader.Value == null)
                            continue;
                        conversation = (string)reader.Value;
                    }
                    else if (reader.Value.Equals("output"))
                    {
                        reader.Read();
                        if (reader.Value == null)
                            continue;
                        return (string)reader.Value;
                    }
                    if (debugResponse)
                        Console.WriteLine("Token: {0}, Value: {1}", reader.TokenType, reader.Value);
                }
                else
                {
                    if (debugResponse)
                        Console.WriteLine("Token: {0}", reader.TokenType);
                }
            }
            return "";
        }
    }
}
