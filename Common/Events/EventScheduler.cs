﻿using Common;
using Common.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Common.EventFetcher;

namespace Common
{
    public class EventScheduler
    {

        static EventScheduler eventScheduler = new EventScheduler();
        public static EventScheduler get() { return eventScheduler; }


        public abstract class ScheduledTask
        {
            public bool executing = false;
            public bool completed = false;
            public DateTime creationDate;
            public DateTime completeDate;
            public ScheduledTask()
            {
                creationDate = DateTime.UtcNow;
            }
            public abstract Task<bool> tryToComplete();
        }

        public class UpdateWebTask : ScheduledTask
        {
            public UpdateWebTask()
            {
            }

            public async override Task<bool> tryToComplete()
            {
                executing = true;
                bool success = await EventFetcher.get().parseData();
                completed = success;
                if (completed)
                    completeDate = DateTime.UtcNow;
                executing = false;
                return success;
            }
        }
        int currentDivision = -1;
        int updateRateMinutes = 60;
        List<ScheduledTask> pendingTasks = new List<ScheduledTask>();
        List<ScheduledTask> completedTasks = new List<ScheduledTask>();


        public EventScheduler()
        {
            updateRateMinutes = Convert.ToInt32(ConfigManager.getGlobal().getConfig("eventUpdateRateMinutes"));
            pendingTasks.Add(new UpdateWebTask());
        }

        async Task executeTask(ScheduledTask task)
        {
            await task.tryToComplete();
            pendingTasks.Add(task);
        }

        public void setDivision(uint division)
        {
            this.currentDivision = (int)division;
        }

        bool containsPendingWebUpdateTask()
        {
            foreach (ScheduledTask t in pendingTasks)
            {
                UpdateWebTask updateTask = (UpdateWebTask)t;
                if (updateTask != null)
                    return true;
            }
            return false;
        }

        DateTime getLatestWebUpdate()
        {
            DateTime currentNewestDate = new DateTime(0);
            foreach(ScheduledTask t in completedTasks)
            {
                UpdateWebTask updateTask = (UpdateWebTask)t;
                if (updateTask == null)
                    continue;
                if(!updateTask.completed)
                {
                    Util.log($"Error! Task not completed but in completed pile! {updateTask}");
                    continue;
                }
                if (updateTask.completeDate > currentNewestDate)
                    currentNewestDate = updateTask.completeDate;
            }
            return currentNewestDate;
        }

        async Task updateWeb()
        {
            DateTime latestUpdate = getLatestWebUpdate();
            DateTime currentTime = DateTime.UtcNow;
            TimeSpan difference = currentTime - latestUpdate;
            if (difference >= TimeSpan.FromMinutes(updateRateMinutes))
            {
                //Update
                if (!containsPendingWebUpdateTask())
                    await executeTask(new UpdateWebTask());
            }
            else if (latestUpdate.Hour != currentTime.Hour)
            {
                //if pending no contain
                //add update
                if (!containsPendingWebUpdateTask())
                    await executeTask(new UpdateWebTask());
            }
        }

        async Task updateTasks()
        {
            for (int i = 0; i < pendingTasks.Count;)
            {
                ScheduledTask t = pendingTasks[i];
                if (t.completed)
                {
                    completedTasks.Add(t);
                    pendingTasks.Remove(t);
                    continue;
                }
                else if(!t.executing)
                {
                    await t.tryToComplete();
                }
                i++;
            }
        }



        public async Task update()
        {
            await updateWeb();
            await updateTasks();
        }
    }
}
