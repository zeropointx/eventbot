﻿using Common;
using Common.Config;
using CommonLibrary.Database;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static CommonLibrary.Database.SQLiteDB;

namespace Common
{
    public class EventFetcher
    {
        static EventFetcher eventFetcher = new EventFetcher();
        public static EventFetcher get() { return eventFetcher; }
        public struct Member
        {
            public enum Attendance
            {
                Unknown,
                Coming,
                Maybe,
                Not
            }
            public string name;
            public Attendance attendance;
            public DateTime rsvpTime;
            public void setRsvpTime(string time)
            {
                double data = double.Parse(time);
                rsvpTime = Util.UnixTimeStampToDateTime(data).AddHours(0);
            }
            public string toString()
            {
                return name + " " + attendance.ToString();
            }
        }
        public class EventComparer : IEqualityComparer<Event>
        {

            public bool Equals(Event x, Event y)
            {
                //Check whether the objects are the same object. 
                if (Object.ReferenceEquals(x, y)) return true;

                //Check whether the products' properties are equal. 
                var firstNotSecond = x.attendingMembers.Except(y.attendingMembers).ToList();
                var secondNotFirst = y.attendingMembers.Except(x.attendingMembers).ToList();
                return (x.division == y.division && !firstNotSecond.Any() && !secondNotFirst.Any() && x.endDate == y.endDate && x.game == y.game &&
                    x.hostingOfficer == y.hostingOfficer && x.id == y.id && x.rawData == y.rawData && x.rawTitle == y.rawTitle && x.startDate == y.startDate && x.title == y.title);
            }

            public int GetHashCode(Event obj)
            {
                //int hashAttendingMembers = obj.attendingMembers == null ? 0 : obj.attendingMembers.GetHashCode();
                //int hashAttendingMembersRawData = obj.attendingMembersRawData == null ? 0 : obj.attendingMembersRawData.GetHashCode();
                //int hashDivision = obj.division.GetHashCode();
                //int hashEndDate = obj.endDate == null ? 0 : obj.endDate.GetHashCode();
                //int hashGame = obj.game == null ? 0 : obj.game.GetHashCode();
                //int hashHostingOfficer = obj.hostingOfficer == null ? 0 : obj.hostingOfficer.GetHashCode();
                int hashId = obj.id.GetHashCode();
                //int hashRawData = obj.rawData == null ? 0 : obj.rawData.GetHashCode();
                //int hashRawTitle = obj.rawTitle == null ? 0 : obj.rawTitle.GetHashCode();
                //int hashStartDate = obj.startDate == null ? 0 : obj.startDate.GetHashCode();
                //int hashTitle = obj.title == null ? 0 : obj.title.GetHashCode();
                return hashId;
                //return hashAttendingMembers ^ hashAttendingMembersRawData ^ hashDivision ^ hashEndDate ^ hashGame ^ hashHostingOfficer ^ hashId ^ hashRawData ^ hashRawTitle ^ hashStartDate ^ hashTitle;
            }
        }
        public class Event : IEquatable<Event>
        {
            public int id = -1;
            public string hostingOfficer = "";
            public string title = "";
            public string rawTitle = "";
            public int division = -1;
            public string game = "";
            public DateTime startDate = new DateTime(0);
            public DateTime endDate = new DateTime(0);
            public List<Member> attendingMembers = new List<Member>();
            public string rawData = "";
            public string attendingMembersRawData = "";

            public Event(Event tEv)
            {
                this.id = tEv.id;
                this.hostingOfficer = tEv.hostingOfficer;
                this.title = tEv.title;
                this.rawTitle = tEv.rawTitle;
                this.division = tEv.division;
                this.game = tEv.game;
                this.startDate = tEv.startDate;
                this.endDate = tEv.endDate;
                this.attendingMembers = new List<Member>(tEv.attendingMembers);
                this.rawData = tEv.rawData;
                this.attendingMembersRawData = tEv.attendingMembersRawData;
            }

            public Event()
            {

            }

            public void setTitle(string rawTitle)
            {
                this.rawTitle = rawTitle;
                Match divisionMatch = Regex.Match(rawTitle, @"^\[DI-(.*)\]\s?(.*)$", RegexOptions.IgnoreCase);
                Match divisionMatch2 = Regex.Match(rawTitle, @"^DI-(.*)\s?(.*)$", RegexOptions.IgnoreCase);
                Match divisionMatch3 = Regex.Match(rawTitle, @"^\[D-(.*)\]\s?(.*)$", RegexOptions.IgnoreCase);
                Match clanMatch = Regex.Match(rawTitle, @"^\[Clan\]\s?(.*)$", RegexOptions.IgnoreCase);
                if (divisionMatch.Success)
                {
                    this.title = divisionMatch.Groups[2].Value;
                    setDivison(divisionMatch.Groups[1].Value);
                }
                else if (divisionMatch2.Success)
                {
                    this.title = divisionMatch2.Groups[2].Value;
                    setDivison(divisionMatch.Groups[1].Value);
                }
                else if (divisionMatch3.Success)
                {
                    this.title = divisionMatch3.Groups[2].Value;
                    setDivison(divisionMatch.Groups[1].Value);
                }
                else if (clanMatch.Success)
                {
                    this.title = clanMatch.Groups[1].Value;
                }
                else
                    this.title = rawTitle;
            }
            public void setDivison(string division)
            {
                this.division = Util.parseDivision(division);
            }
            public void setGame(string game)
            {
                this.game = game;
            }
            public void setStartDate(string date)
            {
                double data = double.Parse(date);
                startDate = Util.UnixTimeStampToDateTime(data).AddHours(-6);
            }
            public void setEndDate(string date)
            {
                double data = double.Parse(date);
                endDate = Util.UnixTimeStampToDateTime(data).AddHours(-6);
            }

            public void setHostingOfficer(string hostingOfficer)
            {
                if (hostingOfficer.ToLower().Contains("error:"))
                    this.hostingOfficer = "Unknown";
                else
                    this.hostingOfficer = hostingOfficer;
            }
            public void setAttendingMembers(string data)
            {
                attendingMembersRawData = data;
                if (data == "")
                    return;
                var splitData = data.Split(',');
                foreach (string memberData in splitData)
                {
                    Match eventUserMatch = Regex.Match(memberData, @"(\w+)(\W)(\d+)(\W)(\d+)(\W)", RegexOptions.IgnoreCase);
                    if (!eventUserMatch.Success)
                        continue;
                    var username = eventUserMatch.Groups[1].Value;
                    var attendance = eventUserMatch.Groups[3].Value;
                    var rsvpDate = eventUserMatch.Groups[5].Value;
                    // var userStatusSplit = memberData[memberData.Length - 2]
                    Member member = new Member();
                    member.name = username;
                    member.attendance = (Member.Attendance)int.Parse("" + attendance);
                    member.setRsvpTime(rsvpDate);
                    attendingMembers.Add(member);
                }
                attendingMembers.Sort((emp1, emp2) => emp1.rsvpTime.CompareTo(emp2.rsvpTime));
            }

            public string getStartTimeString(float timezone = 0, bool showUTC = true)
            {
                if (DateTime.UtcNow >= startDate)
                    return "";
                DateTime localStartTime = startDate.AddMinutes(60 * timezone);
                string returnString = "";
                System.TimeSpan difference = (localStartTime - DateTime.UtcNow);
                string dayofweek = localStartTime.DayOfWeek.ToString();
                int days = difference.Days;
                int hours = difference.Hours;
                int minutes = difference.Minutes;

                if (days > 1)
                    returnString = $"{days} days at {dayofweek}";
                else if (days == 1)
                    returnString = $"1 day at {dayofweek}";
                else if (hours > 1)
                    returnString = $"{hours} hours";
                else if (hours == 1)
                    returnString = $"1 hour";
                else
                    returnString = $"{minutes} minutes";

                string timezoneModifier = timezone >= 0 ? "+" : "";
                if(showUTC)
                    return $"{returnString} {localStartTime.ToString("dd/MM/yyyy hh:mm tt")} UTC: {timezoneModifier }{timezone}";
                else
                    return $"{returnString} {localStartTime.ToString("dd/MM/yyyy hh:mm tt")}";
            }

            public string toString(float timezone = 0)
            {
                string coreString = String.Format($"{title} [id:{id}] hostingOfficer: \"{hostingOfficer}\" ");
                string time = "";
                string division = $"[{Util.divisionToString(this.division)}] ";
                if (DateTime.UtcNow < startDate)
                {
                    System.TimeSpan difference = (startDate - DateTime.UtcNow);
                    string dayofweek = startDate.DayOfWeek.ToString();
                    int days = difference.Days;
                    int hours = difference.Hours;
                    int minutes = difference.Minutes;
                    time += $"Event starts in {getStartTimeString(timezone)}.";
                }
                else if (DateTime.UtcNow < endDate)
                {
                    System.TimeSpan difference = (endDate - DateTime.UtcNow);
                    int hours = difference.Hours;
                    int minutes = difference.Minutes;
                    if (hours > 1)
                        time = $"Event is going on and ends in {hours} hours.";
                    else if (hours == 1)
                        time = "Event is going on and ends in 1 hour.";
                    else
                        time = $"Event is going on and ends in {minutes} minutes.";
                }
                else
                {
                    return $"Error getting event data please report! {division} {coreString} {startDate} {endDate}";
                }
                return division + coreString + time;
            }
            public string toDescriptionString()
            {
                string firstHeader = $"{titleWithLink()}";
                string secondHeader = $"hostingOfficer: {hostingOfficer}";
                string time = "";
                if (DateTime.UtcNow < startDate)
                {
                    System.TimeSpan difference = startDate - DateTime.UtcNow;
                    string dayofweek = startDate.DayOfWeek.ToString();
                    int days = difference.Days;
                    int hours = difference.Hours;
                    int minutes = difference.Minutes;
                    time += $"Starts in {getStartTimeString(0, false)}.";
                }
                else if (DateTime.UtcNow < endDate)
                {
                    System.TimeSpan difference = endDate - DateTime.UtcNow;
                    int hours = difference.Hours;
                    int minutes = difference.Minutes;
                    if (hours > 1)
                        time = $"In progress and ends in {hours} hours.";
                    else if (hours == 1)
                        time = "In progress and ends in 1 hour.";
                    else
                        time = $"In progress and ends in {minutes} minutes.";
                }
                else
                {
                    return $"Error getting event data please report! {this}";
                }
                return $"{firstHeader}\n[list][*]{secondHeader}\n[*]{time}[/list]";
            }
            public string titleWithLink()
            {
                string baseUrl = "https://di.community/calendar/event/";
                string titleParsed = rawTitle.Replace("[", "");
                titleParsed = titleParsed.Replace("]", "");
                titleParsed = titleParsed.Replace(" ", "-");
                titleParsed = titleParsed.ToLower();
                return $"[url={baseUrl}{id}-{titleParsed}]{Util.divisionToString(this.division)} {title}[id:{id}][/url]";
            }

            public void setId(string text)
            {
                this.id = int.Parse(text);
            }

            public static bool operator ==(Event obj1, Event obj2)
            {
                if (ReferenceEquals(obj1, obj2))
                {
                    return true;
                }

                if (ReferenceEquals(obj1, null))
                {
                    return false;
                }
                if (ReferenceEquals(obj2, null))
                {
                    return false;
                }
                var firstNotSecond = obj1.attendingMembers.Except(obj2.attendingMembers).ToList();
                var secondNotFirst = obj2.attendingMembers.Except(obj1.attendingMembers).ToList();
                return (obj1.division == obj2.division && !firstNotSecond.Any() && !secondNotFirst.Any() && obj1.endDate == obj2.endDate && obj1.game == obj2.game &&
                    obj1.hostingOfficer == obj2.hostingOfficer && obj1.id == obj2.id && obj1.rawData == obj2.rawData && obj1.rawTitle == obj2.rawTitle && obj1.startDate == obj2.startDate && obj1.title == obj2.title);
            }
            public static bool operator !=(Event obj1, Event obj2)
            {
                return !(obj1 == obj2);
            }
            public bool Equals(Event other)
            {
                if (ReferenceEquals(null, other))
                {
                    return false;
                }
                if (ReferenceEquals(this, other))
                {
                    return true;
                }
                var firstNotSecond = attendingMembers.Except(other.attendingMembers).ToList();
                var secondNotFirst = other.attendingMembers.Except(attendingMembers).ToList();
                if (other.division == division && !firstNotSecond.Any() && !secondNotFirst.Any() && other.endDate == endDate && other.game == game &&
                    other.hostingOfficer == hostingOfficer && other.id == id && other.rawData == rawData && other.rawTitle == rawTitle && other.startDate == startDate && other.title == title)
                    return true;
                return false;
            }
            public override int GetHashCode()
            {
                int hashAttendingMembers = attendingMembers == null ? 0 : attendingMembers.GetHashCode();
                int hashAttendingMembersRawData = attendingMembersRawData == null ? 0 : attendingMembersRawData.GetHashCode();
                int hashDivision = division.GetHashCode();
                int hashEndDate = endDate == null ? 0 : endDate.GetHashCode();
                int hashGame = game == null ? 0 : game.GetHashCode();
                int hashHostingOfficer = hostingOfficer == null ? 0 : hostingOfficer.GetHashCode();
                int hashId = id.GetHashCode();
                int hashRawData = rawData == null ? 0 : rawData.GetHashCode();
                int hashRawTitle = rawTitle == null ? 0 : rawTitle.GetHashCode();
                int hashStartDate = startDate == null ? 0 : startDate.GetHashCode();
                int hashTitle = title == null ? 0 : title.GetHashCode();
                return hashAttendingMembers ^ hashAttendingMembersRawData ^ hashDivision ^ hashEndDate ^ hashGame ^ hashHostingOfficer ^ hashId ^ hashRawData ^ hashRawTitle ^ hashStartDate ^ hashTitle;
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj))
                {
                    return false;
                }
                if (ReferenceEquals(this, obj))
                {
                    return true;
                }

                return obj.GetType() == GetType() && Equals((Event)obj);
            }
            public string notEqualsReason(Event other)
            {
                var firstNotSecond = attendingMembers.Except(other.attendingMembers).ToList();
                var secondNotFirst = other.attendingMembers.Except(attendingMembers).ToList();
                if (other.division == division)
                    return "division";
                else if (!firstNotSecond.Any() || !secondNotFirst.Any())
                    return "attendingMembers";
                else if (other.endDate == endDate)
                    return "enddate";
                else if (other.game == game)
                    return "game";
                else if (other.hostingOfficer == hostingOfficer)
                    return "hostingOfficer";
                else if (other.id == id)
                    return "id";
                else if (other.rawData == rawData)
                    return "rawData";
                else if (other.rawTitle == rawTitle)
                    return "rawTitle";
                else if (other.startDate == startDate)
                    return "startDate";
                else if (other.title == title)
                    return "title";
                return "none";
            }
        }
        enum EventState
        {
            EventNotStarted,
            EventInProgress,
            EventComplete
        }
        public EventFetcher()
        {

        }
        public Event[] getAllEvents()
        {
            Event[] dbEvents = SQLiteDB.get().getEvents(null);
            return dbEvents;
        }
        //Dictionary<int, Event> events = new Dictionary<int, Event>();
        public Event[] getDivisionEvents(uint[] divisions)
        {
            int[] divIntAr = new int[divisions.Length];
            for (int i = 0; i < divIntAr.Length; i++) divIntAr[i] = (int)divisions[i];

            DBParam[] dbParams = { new DBParam(DBParamType.DIVISION, divIntAr.Cast<object>().ToArray(),DBOperator.EQUALS), new DBParam(DBParamType.START_TIME,new object[] {DateTime.UtcNow}, DBOperator.GREATER_THAN) };
            Event[] dbEvents = SQLiteDB.get().getEvents(dbParams);

            List<Event> tempEventList = new List<Event>();
            foreach (Event e in dbEvents)
            {
                if (DateTime.UtcNow > e.startDate)
                    continue;
                foreach (int div in divisions)
                {
                    if (e.division == div)
                    {
                        tempEventList.Add(e);
                        break;
                    }
                }
            }
            return tempEventList.ToArray();
        }
        public Event getEvent(int id)
        {
            DBParam[] dbParams = { new DBParam(DBParamType.ID, new object[] { id }, DBOperator.EQUALS) };
            Event[] dbEvents = SQLiteDB.get().getEvents(dbParams);
            if (dbEvents.Length > 0)
                return dbEvents[0];
            return null;
        }
        public Event getNextEvent()
        {
            DBParam[] dbParams = { new DBParam(DBParamType.START_TIME, new object[] { DateTime.UtcNow }, DBOperator.GREATER_THAN) };
            Event[] dbEvents = SQLiteDB.get().getEvents(dbParams);
            if (dbEvents.Length > 0)
                return dbEvents[0];
            return null;
        }
        public Event getNextEvent(uint[] divisions)
        {
            DBParam[] dbParams = { new DBParam(DBParamType.DIVISION, divisions.Cast<object>().ToArray(), DBOperator.EQUALS), new DBParam(DBParamType.START_TIME, new object[] { DateTime.UtcNow }, DBOperator.GREATER_THAN) };
            Event[] dbEvents = SQLiteDB.get().getEvents(dbParams);
            if(dbEvents.Length > 0)
                return dbEvents[0];
            return null;
        }
        public Event[] getNextEventList(uint[] divisions, uint days, uint maxEvents)
        {
            DBParam[] dbParams = { new DBParam(DBParamType.DIVISION, divisions.Cast<object>().ToArray(), DBOperator.EQUALS), new DBParam(DBParamType.START_TIME, new object[] { DateTime.UtcNow }, DBOperator.GREATER_THAN) };
            Event[] dbEvents = SQLiteDB.get().getEvents(dbParams);
            List<Event> tempEvents = new List<Event>();
            foreach (Event e in dbEvents)
            {
                if ((e.startDate - DateTime.UtcNow).Days <= days && tempEvents.Count <= maxEvents)
                {
                    tempEvents.Add(e);
                    break;
                }
            }
            return tempEvents.ToArray();
        }
        public bool containsEvent(string username)
        {
            return getNextEvent(username) != null;
        }
        public Event[] getNextEventList(uint days, uint maxEvents, string username)
        {
            DBParam[] dbParams = { new DBParam(DBParamType.START_TIME, new object[] { DateTime.UtcNow }, DBOperator.GREATER_THAN) };
            DBParam memberParam = new DBParam(DBParamType.ATTENDING_MEMBERS, new object[] { $"%username" }, DBOperator.LIKE);
            Event[] dbEvents = SQLiteDB.get().getEvents(dbParams);
            List<Event> tempEvents = new List<Event>();
            foreach (Event e in dbEvents)
            {
                foreach (Member member in e.attendingMembers)
                {
                    if ((e.startDate - DateTime.UtcNow).Days <= days && tempEvents.Count <= maxEvents)
                    {
                        if (username.ToLower().StartsWith(member.name.ToLower()) && (member.attendance == Member.Attendance.Coming || member.attendance == Member.Attendance.Maybe))
                            tempEvents.Add(e);
                    }
                }
            }
            return tempEvents.ToArray();
        }
        public Event getNextEvent(string username)
        {
            DBParam[] dbParams = { new DBParam(DBParamType.START_TIME, new object[] { DateTime.UtcNow }, DBOperator.GREATER_THAN) };
            DBParam memberParam = new DBParam(DBParamType.ATTENDING_MEMBERS, new object[] { $"%username" }, DBOperator.LIKE);
            Event[] dbEvents = SQLiteDB.get().getEvents(dbParams);
            foreach (Event e in dbEvents)
            {
                foreach (Member member in e.attendingMembers)
                {
                    if (username.ToLower().StartsWith(member.name.ToLower()) && (member.attendance == Member.Attendance.Coming || member.attendance == Member.Attendance.Maybe))
                        return e;
                }
            }
            return null;
        }
        public async Task<bool> parseData()
        {
            try
            {
                string url = (string)ConfigManager.getGlobal().getConfig("updateUrl");
                HtmlWeb web = new HtmlWeb();
                
                HttpClient httpClient = new HttpClient() { MaxResponseContentBufferSize = 1000000 };
                HttpResponseMessage data = await httpClient.GetAsync(url);
                Stream stream = await data.Content.ReadAsStreamAsync();
                HtmlDocument doc = new HtmlDocument();
                doc.Load(stream);
                stream.Close();
                httpClient.Dispose();
                List<Event> events = new List<Event>();
                foreach (HtmlNode table in doc.DocumentNode.SelectNodes("//table"))
                {
                    //Console.WriteLine("Found: " + table.Id);
                    for (int i = 0; i < table.SelectNodes("tr").Count; i++)
                    {
                        if (i == 0)
                            continue;
                        HtmlNode row = table.SelectNodes("tr")[i];
                        // Console.WriteLine("row");
                        Event eve = new Event();

                        for (int j = 0; j < row.SelectNodes("th|td").Count; j++)
                        {

                            HtmlNode cell = row.SelectNodes("th|td")[j];
                            string text = cell.InnerText;

                            switch (j)
                            {
                                case 0:
                                    eve.setId(text);
                                    break;
                                case 1:
                                    eve.setGame(text);
                                    break;
                                case 2:
                                    eve.setHostingOfficer(text);
                                    break;
                                case 3:
                                    eve.setTitle(text);
                                    break;
                                case 4:
                                    eve.setStartDate(text);
                                    break;
                                case 5:
                                    eve.setEndDate(text);
                                    break;
                                case 6:
                                    eve.setAttendingMembers(text);
                                    break;
                                case 7:
                                    eve.rawData = text;
                                    break;
                            }
                            //Console.WriteLine("cell: " + cell.InnerText);
                        }
                        events.Add(eve);

                    }
                }
                foreach (Event newEvent in events)
                {
                    Event dbEvent = SQLiteDB.get().getEvent(newEvent.id);
                    if(dbEvent == null)
                    {
                        SQLiteDB.get().insertEvent(newEvent);
                    }
                    else if(!newEvent.Equals(dbEvent))
                    {
                        SQLiteDB.get().updateEvent(newEvent);
                    }
                }
                Util.log("Done loading webpage!");
                GC.Collect();
                return true;

            }
            catch (Exception e)
            {
                Util.log($"{e}");
                return false;
            }
        }
    }
}
