﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static Common.EventFetcher;
using static Common.PermissionManager;

namespace Common
{
    public class BotPlatform
    {
        public enum PlatformType
        {
            Discord,
            Teamspeak
        }
        public Object platform = null;
        public PlatformType platformType;
        public BotPlatform(Object platform,PlatformType platformType)
        {
            this.platform = platform;
            this.platformType = platformType;
        }
    }
    public abstract class MessageData
    {
        public string command;
        public string[] args;
        public string senderId;
        public string senderName;
        public string senderUid;
        public bool privateMessage;
        public BotPlatform platform;
        public abstract Task sendPrivateResponse(string message);
        public abstract Task sendChannelResponse(string message);
        public BotPlatform.PlatformType getPlatform()
        {
            return platform.platformType;
        }
        public async Task sendResponse(string message)
        {
            if (privateMessage)
                await sendPrivateResponse(message);
            else
                await sendChannelResponse(message);
        }
        public abstract UserData getUserData();
    }
    public class CommandManager
    {
        static CommandManager commandManager = new CommandManager();
        public static CommandManager get() { return commandManager; }
        public abstract class Command
        {
            public string[] keywords;
            public string description;
            public string usage;
            public Command()
            {
            }

            public virtual async Task handle(MessageData msg)
            {
                
            }
            public virtual bool hasPermission(MessageData msg)
            {
                return true;
            }
            public async Task sendResponse(string message, MessageData msg)
            {
                if(msg.privateMessage)
                    await msg.sendPrivateResponse(message);
                else
                    await msg.sendChannelResponse(message);
            }
        }

        class NextEvent : Command
        {
            public NextEvent() : base()
            {
                keywords = new string[] { "nextevent", "ne"};
                description = "Prints next event in your division";
                usage = $"!{keywords[0]} division or just !{keywords[0]}";
            }
            public async override Task handle(MessageData msg)
            {
                await base.handle(msg);
                if (msg.args != null && msg.args.Length != 1 && msg.args.Length != 2)
                {
                    await sendResponse($"Invalid command format. Do {usage}", msg);
                    return;
                }
                UserData userData = msg.getUserData();
                if (userData == null)
                {
                    await sendResponse($"Run into internal error in command {keywords[0]}", msg);
                    return;
                }
                uint division = userData.division;
                if (msg.args != null && msg.args.Length == 2)
                {
                    int divisionNumber = Common.Util.divisionStringToInt(msg.args[1]);
                    if (divisionNumber != -1)
                        division = (uint)divisionNumber;
                    else
                    {
                        await sendResponse($"Please give proper division. {usage}", msg);
                        return;
                    }
                }
                Event nextEvent = Common.EventFetcher.get().getNextEvent(new uint[] { division,0 });
                if(nextEvent == null)
                {
                    await sendResponse($"Could not find next event for division {Common.Util.divisionToString((int)division)}", msg);
                }
                else
                {
                    await sendResponse(nextEvent.toString(userData.timeZone), msg);
                    if (!userData.timezoneSet)
                        await sendResponse($"You have not set your timezone. If you want more accurate times do !timezone number (and to get rid of this message)", msg);
                }
                    
            }
        }

        class ListEvents : Command
        {
            public ListEvents() : base()
            {
                keywords = new string[] { "listevents", "le" };
                description = "Lists X events for x days (you can configure these values)";
                usage = $"!{keywords[0]} or !{keywords[0]} 7days 8events";
            }
            public async override Task handle(MessageData msg)
            {
                await base.handle(msg);
                UserData userData = msg.getUserData();
                if (userData == null)
                {
                    await sendResponse($"Run into internal error in command {keywords[0]}", msg);
                    return;
                }
                if (msg.args == null || msg.args.Length == 1 || msg.args.Length == 2)
                {
                    if(msg.args != null)
                    {
                        foreach(string arg in msg.args)
                        {
                            Match daysMatch = Regex.Match(arg, @"^(\d+)days$", RegexOptions.IgnoreCase);
                            Match eventsMatch = Regex.Match(arg, @"^(\d+)events$", RegexOptions.IgnoreCase);
                            if (daysMatch.Success)
                            {
                                uint days;
                                bool isNumber = uint.TryParse(daysMatch.Groups[1].Value, out days);
                                userData.listEventsDays = days;
                                PermissionManager.get().writeFile();
                            }
                            else if(eventsMatch.Success)
                            {
                                uint days;
                                bool isNumber = uint.TryParse(eventsMatch.Groups[1].Value, out days);
                                userData.maxListEvents = days;
                                PermissionManager.get().writeFile();
                            }
                            else
                            {
                                await sendResponse($"Invalid argument {arg}. Usage: {usage}", msg);
                                return;
                            }
                        }
                    }
                    Event[] nextEvents = Common.EventFetcher.get().getNextEventList(new uint[] { userData.division }, userData.listEventsDays, userData.maxListEvents);
                    string sendString = $"Next events within your division for next {userData.listEventsDays}days ({userData.maxListEvents})\n";
                    foreach (Event e in nextEvents)
                    {
                        sendString += $"{e.toString(userData.timeZone)}\n";
                        if(msg.getPlatform() == BotPlatform.PlatformType.Teamspeak)
                        {
                            if(sendString.Length > 900)
                            {
                                await sendResponse(sendString, msg);
                                sendString = "\n";
                            }
                        }
                    }
                    await sendResponse(sendString, msg);
                }
            }
        }
        class ListMyEvents : Command
        {
            public ListMyEvents() : base()
            {
                keywords = new string[] { "listmyevents", "lme" };
                description = "Lists X events for x days (you can configure these values) that you have rsvp'd in";
                usage = $"!{keywords[0]} or !{keywords[0]} days=7 events=8";
            }
            public async override Task handle(MessageData msg)
            {
                await base.handle(msg);

                UserData userData = msg.getUserData();
                if (userData == null)
                {
                    await sendResponse($"Run into internal error in command {keywords[0]}", msg);
                    return;
                }
                if (msg.args == null || msg.args.Length == 1 || msg.args.Length == 2)
                {
                    if (msg.args != null)
                    {
                        foreach (string arg in msg.args)
                        {
                            Match daysMatch = Regex.Match(arg, @"^days=(\d+)$", RegexOptions.IgnoreCase);
                            Match eventsMatch = Regex.Match(arg, @"^events=(\d+)$", RegexOptions.IgnoreCase);
                            if (daysMatch.Success)
                            {
                                uint days;
                                bool isNumber = uint.TryParse(daysMatch.Groups[1].Value, out days);
                                userData.listMyEventsDays = days;
                                PermissionManager.get().writeFile();
                            }
                            else if (eventsMatch.Success)
                            {
                                uint days;
                                bool isNumber = uint.TryParse(eventsMatch.Groups[1].Value, out days);
                                userData.maxListMyEvents = days;
                                PermissionManager.get().writeFile();
                            }
                            else
                            {
                                await sendResponse($"Invalid argument {arg}. Usage: {usage}", msg);
                                return;
                            }
                        }
                    }
                    Event[] nextEvents = Common.EventFetcher.get().getNextEventList(userData.listEventsDays, userData.maxListEvents,msg.senderName);
                    string sendString = $"Next events that you have rsv'd in for next {userData.listEventsDays} days (maxEvents:{userData.maxListEvents})\n";
                    foreach (Event e in nextEvents)
                    {
                        sendString += $"{e.toString(userData.timeZone)}\n";
                        if (msg.getPlatform() == BotPlatform.PlatformType.Teamspeak)
                        {
                            if (sendString.Length > 900)
                            {
                                await sendResponse(sendString, msg);
                                sendString = "\n";
                            }
                        }
                    }
                    await sendResponse(sendString, msg);
                }

            }
        }
        class MyNextEvent : Command
        {
            public MyNextEvent() : base()
            {
                keywords = new string[]{ "mynextevent", "mne"};
                description = "Prints out next event that you have RSVP'd in";
            }
            public async override Task handle(MessageData msg)
            {
                await base.handle(msg);

                UserData userData = msg.getUserData();
                if (userData == null)
                {
                    await sendResponse($"Run into internal error in command {keywords[0]}", msg);
                    return;
                }
                Event nextEvent = Common.EventFetcher.get().getNextEvent(msg.senderName);
                if (nextEvent == null)
                {
                    await sendResponse($"Could not find next event that you have RSVP'd in", msg);
                }
                else
                {
                    await sendResponse(nextEvent.toString(userData.timeZone), msg);
                    if (!userData.timezoneSet)
                        await sendResponse($"You have not set your timezone. If you want more accurate time do !timezone number (and to get rid of this message)", msg);
                }
                   
            }
        }
        class Help : Command
        {
            public Help() : base()
            {
                keywords = new string[]{ "help", "h"};
                description = "prints out all commands and gives general info";
            }
            public async override Task handle(MessageData msg)
            {
                await base.handle(msg);
    
                string commandString = "";
                foreach (Command c in get().commands)
                {
                    if (!c.hasPermission(msg))
                        continue;
                    string tabString = "\t";
                    int max = 25;
                    for (int i = 0; i < (max - c.keywords[0].Length) / 3; i++)
                    {
                        tabString += "\t";
                    }
                    commandString += $"[b]{c.keywords[0]}[/b]{tabString}{c.description}\n";
                }
                commandString = commandString.Substring(0, commandString.Length - 1);
                string helpResponse = $@"
--------------------------------
Following commands are available to you. Write these to channel or as private message to bot
{commandString}
--------------------------------
";
                await sendResponse(helpResponse, msg);
            }
        }

        class AddPermission : Command
        {
            public AddPermission() : base()
            {
                keywords = new string[] { "addpermission" , "ap"};
                description = "Test";
            }
            public async override Task handle(MessageData msg)
            {
                await base.handle(msg);
                if (msg.args == null || msg.args.Length != 3)
                {
                    await sendResponse("Invalid command format. Do !addpermission permission value", msg);
                    return;
                }


                Common.PermissionManager.get().addPermission(msg.senderUid, msg.args[1], msg.args[2]);
                await sendResponse($"Added permission {msg.args[1]} {msg.args[2]}.", msg);
            }
            public override bool hasPermission(MessageData msg)
            {
                UserData userData = msg.getUserData();
                if (userData == null)
                    return false;
                return userData.hasPermission("addpermission", "true");
            }
        }
        class GivePermission : Command
        {
            public GivePermission() : base()
            {
                keywords = new string[] { "givepermission", "gp" };
                description = "Test";
            }
            public async override Task handle(MessageData msg)
            {
                await base.handle(msg);
                if (msg.args == null || msg.args.Length != 3)
                {
                    await sendResponse($"Invalid command format. Do {keywords[0]} permission value", msg);
                    return;
                }
                //await telnet.getClientList();
            }
            public override bool hasPermission(MessageData msg)
            {
                UserData userData = msg.getUserData();
                if (userData == null)
                    return false;
                return userData.hasPermission("givepermission", "true");
            }
        }
        class SetDivision : Command
        {
            public SetDivision() : base()
            {
                keywords = new string[] { "division","d" };
                description = "Set your main division. !setdivision v or !setdivision 5";
            }
            public async override Task handle(MessageData msg)
            {
                await base.handle(msg);
                if (msg.args == null || msg.args.Length != 1)
                {
                    await sendResponse($"Invalid command format. Please do !{keywords[0]} 1,2,3,4,5 or DI-V or V instead", msg);
                    return;
                }
                int division = Common.Util.parseDivision(msg.args[0]);
                if (division < 0)
                {
                    await sendResponse($"Invalid division, please do !{keywords[0]} 1,2,3,4,5 or DI-V or V instead", msg);
                    return;
                }
                Common.PermissionManager.get().setDivision(msg.senderUid,(uint)division);
                await sendResponse($"Division {division} set.", msg);
            }
            public override bool hasPermission(MessageData msg)
            {
                return true;
            }
        }
        class SetTimezone : Command
        {
            public SetTimezone() : base()
            {
                keywords = new string[] { "timezone", "t"};
                description = $"Set your timezone (UTC) so bot can give exact startTime. Usage: !{keywords[0]} -3";
            }
            public async override Task handle(MessageData msg)
            {
                await base.handle(msg);
                if (msg.args == null || msg.args.Length != 2)
                {
                    await sendResponse($"{description}",msg);
                    return;
                }
                int timezone;
                bool number = int.TryParse(msg.args[1], out timezone);
                if(!number || timezone > 12 ||timezone < -12)
                {
                    await sendResponse($"Invalid timezone, please !{keywords[0]} number", msg);
                    return;
                }
                Common.PermissionManager.get().setTimezone(msg.senderUid, timezone);
                await sendResponse($"Timezone {Common.Util.timezoneString(timezone)} set.", msg);
            }
            public override bool hasPermission(MessageData msg)
            {
                return true;
            }
        }
        class Settings : Command
        {
            public Settings() : base()
            {
                keywords = new string[] { "settings", "s" };
                description = $"Show your client's settings.";
                usage = $"!{keywords[0]}";
            }
            public async override Task handle(MessageData msg)
            {
                await base.handle(msg);
                UserData userData = msg.getUserData();
                if (userData == null)
                {
                    await sendResponse($"Run into internal error in command {keywords[0]}", msg);
                    return;
                }
                string response = $@"Settings
--------------------------------
Timezone\t\t\t{Common.Util.timezoneString(userData.timeZone)}
Superadmin\t\t{userData.superAdmin}
CountryCode\t\t{userData.countryCode}
TimezoneSet\t\t{userData.timezoneSet}
Division\t\t\t\t{Common.Util.divisionToString((int)userData.division)}";
                if (userData.permissions.Count > 0)
                    response += "\r\nExtra permissions:\r\n";
                foreach (var permission in userData.permissions)
                {
                    response += $"{permission.name}\t\t{permission.value}\r\n";
                }
                response += "\r\n--------------------------------";
                await sendResponse(response, msg);
            }
            public override bool hasPermission(MessageData msg)
            {
                return true;
            }
        }
        class Talk : Command
        {
            public Talk() : base()
            {
                keywords = new string[] { "talk", "ta" };
                description = $"Toggle talking normally to bot !{keywords[0]} ";
            }
            public async override Task handle(MessageData msg)
            {
                await base.handle(msg);
                UserData userData = msg.getUserData();
                if (userData == null)
                {
                    await sendResponse($"Run into internal error in command {keywords[0]}", msg);
                    return;
                }
                userData.talkMode = !userData.talkMode;
                Common.PermissionManager.get().writeFile();
                string talkModeString = "";
                if (userData.talkMode)
                    talkModeString = "Bot will now have discussion with you.";
                else
                    talkModeString = "Bot will only respond to commands now.";
                await sendResponse($"{talkModeString}", msg);
            }
            public override bool hasPermission(MessageData msg)
            {
                return true;
            }
        }
        class Register : Command
        {
            public Register() : base()
            {
                keywords = new string[] { "register", "r" };
                description = $"Register Teamspeak to Discord.";

            }
            public async override Task handle(MessageData msg)
            {
                await base.handle(msg);
                if (msg.platform.platformType == BotPlatform.PlatformType.Teamspeak)
                {
                    UserData userData = Common.PermissionManager.get().getUserByTsUid(msg.senderUid);
                    if (userData != null)
                    {
                        if (msg.args != null)
                        {
                            await sendResponse("Please just type !register in teamspeak", msg);
                            if (userData.registerData != "" && msg.args.Contains(userData.registerData))
                            {
                                await msg.sendPrivateResponse("Because you included your secret code in channel chat your secret code has been reset. Please do !register in teamspeak again!");
                                userData.registerData = "";
                                Common.PermissionManager.get().writeFile();
                            }
                            return;
                        }
                        if (userData.discordId != "")
                        {
                            await sendResponse($"Already registered", msg);
                            return;
                        }
                        string randomData = "";
                        Random r = new Random();
                        for (int i = 0; i < 10; i++)
                        {
                            randomData += r.Next(10, 99);
                        }
                        userData.registerData = randomData;
                        Common.PermissionManager.get().writeFile();
                        await msg.sendPrivateResponse($"Your secret number is {randomData}. Please type !register number as private message to bot in discord.");
                    }
                }
                else if (msg.platform.platformType == BotPlatform.PlatformType.Discord)
                {
                    if (msg.args == null || msg.args.Length != 1)
                    {
                        await sendResponse("Type !register secretcode. If you have not run !register on teamspeak run that first.", msg);
                        return;
                    }
                    UserData userData = Common.PermissionManager.get().getUserByRegisterData(msg.args[0]);
                    if(userData == null)
                    {
                        await msg.sendPrivateResponse($"Please check your code again.");
                    }
                    else if (msg.args[0] == userData.registerData)
                    {
                        userData.registerData = "";
                        userData.discordId = msg.senderId;
                        Common.PermissionManager.get().writeFile();
                        await msg.sendPrivateResponse($"Your teamspeak has now been linked to your discord.");
                    }
                }
            }
            public override bool hasPermission(MessageData msg)
            {
                return true;
            }
        }
        public class Cat : Command
        {
            static public EventHandler<MessageData> Execute;
            public Cat() : base()
            {
                keywords = new string[] { "cat", "c" };
                description = $"Cat !{keywords[0]} ";
            }
            public async override Task handle(MessageData msg)
            {
                await base.handle(msg);
                Execute?.Invoke(this, msg);
                //
            }
            public override bool hasPermission(MessageData msg)
            {
                return msg.getPlatform() == BotPlatform.PlatformType.Discord;
            }
        }
        public class Debug : Command
        {
            public Debug() : base()
            {
                keywords = new string[] { "debug", "d" };
                description = $"Debug !{keywords[0]} ";
            }
            public async override Task handle(MessageData msg)
            {
                await base.handle(msg);
                Event[] events = Common.EventFetcher.get().getAllEvents();
                Event[] divEvents = Common.EventFetcher.get().getDivisionEvents(new uint[] { 15 });

                await sendResponse($"Available events: {events.Length} div events: {divEvents.Length}", msg);
                //
            }
            public override bool hasPermission(MessageData msg)
            {
                UserData userData = msg.getUserData();
                if (userData == null)
                    return false;
                return userData.superAdmin;
            }
        }
        public class RSVP : Command
        {
            public RSVP() : base()
            {
                keywords = new string[] { "rsvp", "r" };
                description = $"Print list of all people that have rsvp'd to event Usage: {usage} ";
                usage = $"!{keywords[0]} eventid";
            }
            public async override Task handle(MessageData msg)
            {
                await base.handle(msg);
                if(msg.args == null || msg.args.Length != 1)
                {
                    await sendResponse($"Wrong format, do {usage}", msg);
                    return;
                }
                int id;
                bool success = int.TryParse(msg.args[0],out id);
                if(!success)
                {
                    await sendResponse($"Id is not number! Please do {usage}", msg);
                    return;
                }
                Event tempEvent = Common.EventFetcher.get().getEvent(id);
                if (tempEvent == null)
                {
                    await sendResponse($"No event found with that id.", msg);
                    return;
                }
                string message = $"Attending members for event {tempEvent.title}";
                foreach (var att in tempEvent.attendingMembers)
                {
                    message += $"\n{att.name} {att.attendance.ToString()} {Util.parseToTeamspeak(att.rsvpTime.ToString())}";
                }
                await sendResponse(message, msg);
            }
            public override bool hasPermission(MessageData msg)
            {
                UserData userData = msg.getUserData();
                if (userData == null)
                    return false;
                return userData.superAdmin;
            }
        }
        public class Cmd : Command
        {
            public Cmd() : base()
            {
                keywords = new string[] { "cmd", "c" };
                description = $"Cmd !{keywords[0]} ";
            }
            static public EventHandler<MessageData> Execute;
            public async override Task handle(MessageData msg)
            {
                await base.handle(msg);
                if (msg.args[0].ToLower().Equals("subchannel"))
                {
                    if(msg.getPlatform() == BotPlatform.PlatformType.Teamspeak && msg.args != null && msg.args.Length == 2)
                        Execute?.Invoke(this, msg);
                    return;
                }
                else
                {
                    Execute?.Invoke(this, msg);
                }
                await sendResponse($"Executed {msg.args[0]}", msg);
            }
            public override bool hasPermission(MessageData msg)
            {
                UserData userData = msg.getUserData();
                if (userData == null)
                    return false;
                return userData.superAdmin;
            }
        }
        List<Command> commands = new List<Command>();
        public CommandManager()
        {
            //Initialize
            createCommands();
        }
        void createCommands()
        {
            //Go get all inherited commands and create an instance from them
            var commandTypes = (from domainAssembly in AppDomain.CurrentDomain.GetAssemblies()
                                from assemblyType in domainAssembly.GetTypes()
                                where typeof(Command).IsAssignableFrom(assemblyType) && !assemblyType.IsAbstract
                                select assemblyType).ToArray();

            foreach (var classType in commandTypes)
            {
                Command instance = (Command)Activator.CreateInstance(classType);
                commands.Add(instance);
            }
        }
        public void handleCommand(MessageData message)
        {
            foreach (Command c in commands)
            {
                foreach(string keyword in c.keywords)
                {
                    if (message.command.Equals(keyword, StringComparison.InvariantCultureIgnoreCase))
                    {
                        if(message.platform.platformType == BotPlatform.PlatformType.Teamspeak)
                            Common.PermissionManager.get().addUser(message.senderUid, message.senderName);
                        else
                        {
                            UserData data = Common.PermissionManager.get().getUserByDID(message.senderId);
                            if(data == null && c.keywords[0] != "register")
                            {
                                message.sendResponse("You must register yourself first! Please type !register on Teamspeak");
                                return;
                            }
                        }
                        if (!c.hasPermission(message))
                        {
                            if(message.privateMessage)
                                message.sendPrivateResponse($"You don't have permission for {c.keywords[0]}");
                            else
                                message.sendPrivateResponse($"You don't have permission for {c.keywords[0]}");
                        }
                        else
                        {
                            try
                            {
                                Util.log($"Command ({message.senderName}): {message.command} {message.args}");
                                c.handle(message);
                            }
                            catch (Exception e)
                            {
                                message.sendPrivateResponse($"Copy this to Kushzei {e.InnerException}");
                            }
                        }
                    }
                }
            
            }
        }
    }
}
