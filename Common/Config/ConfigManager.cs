﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Common.Config
{
    public class ConfigManager
    {
        const string globalFilePath = "../CFG/GlobalSettings.json";
        static ConfigManager manager = null;
        public static ConfigManager get() { return manager; }
        static GlobalConfig globalConfig = null;
        public static GlobalConfig getGlobal() { return globalConfig; }

        public Dictionary<string, TSBotInstanceConfig> tsbotInstanceConfigs = new Dictionary<string, TSBotInstanceConfig>();
        public ConfigManager()
		{
            globalConfig = new GlobalConfig();
            bool fileExists = File.Exists(globalFilePath);
            if (fileExists)
                readGlobalConfigFile();
            else
                writeGlobalConfigFile();
            writeBotInstanceConfigs();
            readBotInstanceConfigs();
        }



        public TSBotInstanceConfig getBotInstanceConfig(string key)
        {
            if (tsbotInstanceConfigs.ContainsKey(key))
                return tsbotInstanceConfigs[key];
            return null;
        }

        public static void initialize()
        {
            globalConfig = new GlobalConfig();
            manager = new ConfigManager();
        }

        void writeBotInstanceConfigs()
        {
            DirectoryInfo dir = new DirectoryInfo((string)ConfigManager.getGlobal().getConfig("botInstanceFolder"));

            foreach (TSBotInstanceConfig i in tsbotInstanceConfigs.Values)
            {
                string filePath = dir + i.id + ".json";
                bool fileExists = File.Exists(filePath);
                if (!fileExists)
                {
                    FileStream stream = File.Create(filePath);
                    stream.Close();
                }

                JsonSerializer serializer = new JsonSerializer();
                serializer.Converters.Add(new JavaScriptDateTimeConverter());
                serializer.NullValueHandling = NullValueHandling.Ignore;
                serializer.Formatting = Formatting.Indented;

                using (StreamWriter sw = new StreamWriter(filePath))
                using(JsonWriter writer = new JsonTextWriter(sw))
                {
                    serializer.Serialize(writer, i);
                }
            }
        }

        void readBotInstanceConfigs()
        {
            DirectoryInfo dir = new DirectoryInfo((string)ConfigManager.getGlobal().getConfig("botInstanceFolder"));
            if (dir == null)
                return;
            foreach(FileInfo f in dir.GetFiles())
            {
                string filePath = f.FullName;
                bool fileExists = File.Exists(filePath);
                if (!fileExists)
                {
                    continue;
                }
                using (System.IO.StreamReader file =
         new System.IO.StreamReader(filePath))
                {
                    JObject o = (JObject)JToken.ReadFrom(new JsonTextReader(file));
                    if (o == null)
                    {
                        continue;
                    }

                    TSBotInstanceConfig deserializedInstance = JsonConvert.DeserializeObject<TSBotInstanceConfig>(o.ToString());
                    tsbotInstanceConfigs.Remove(deserializedInstance.id);
                    tsbotInstanceConfigs.Add(deserializedInstance.id, deserializedInstance);
                }
            }
        }


        void readGlobalConfigFile()
        {
            bool fileExists = File.Exists(globalFilePath);
            if (!fileExists)
            {
                return;
            }
            using (System.IO.StreamReader file =
     new System.IO.StreamReader(globalFilePath))
            {
                JObject o = (JObject)JToken.ReadFrom(new JsonTextReader(file));
                if (o.Count != 1)
                {
                    Util.log("Error reading permissions! Invalid object count");
                    return;
                }
                JToken[] tokenArray = o.First.Values().ToArray();
                JObject.FromObject(o);
                foreach (JToken token in tokenArray)
                {
                    KeyValuePair<string, Object> data = token.ToObject<KeyValuePair<string, Object>>();
                    globalConfig.setConfig(data.Key, data.Value);
                }
            }
        }
        public void writeGlobalConfigFile()
        {
            bool fileExists = File.Exists(globalFilePath);
            if (!fileExists)
            {
                FileStream stream = File.Create(globalFilePath);
                stream.Close();
            }
            JArray array = new JArray();
            foreach (var u in globalConfig.getValues())
            {
                array.Add(JObject.FromObject(u));
            }

            JObject o = new JObject();
            o["ConfigArray"] = array;

            string json = o.ToString();
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(globalFilePath, false))
            {
                file.WriteLine(json);
            }
        }
    }
}
