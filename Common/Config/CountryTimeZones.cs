﻿using Common.Config;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Common
{
    public class CountryTimeZones
    {
        static CountryTimeZones countryTimezone = null;
        public static CountryTimeZones get()
        {
            if (countryTimezone == null)
                return new CountryTimeZones();
            return countryTimezone;
        }
        public struct CountryTimeZone
        {
            public string country;
            public string countryCode;
            public TimeSpan timezoneOffset;
            public void setTimezone(string sign, string hours, string minutes)
            {
                timezoneOffset = new TimeSpan(int.Parse(hours), int.Parse(minutes),0);
                if (sign.Equals("−"))
                    timezoneOffset = timezoneOffset.Negate();
            }
            public override string ToString()
            {
                return $"CountryCode: {countryCode} Country: {country} timezoneOffset {timezoneOffset}";
            }
            public void setCountry(string country)
            {
                this.country = country;
            }
        }
        List<CountryTimeZone> countryTimezones = new List<CountryTimeZone>();
        public CountryTimeZones()
        {
            readFile();
        }
        void readFile()
        {
            string filePath = (string)ConfigManager.getGlobal().getConfig("timezoneFile");
            bool fileExists = File.Exists(filePath);
            if (!fileExists)
            {
                return;
            }
            string[] lines = File.ReadAllLines(filePath, Encoding.UTF8);
            foreach (string line in lines)
            {
                //(\D)(/d+):(/d+)
                Match countryMatch = Regex.Match(line, @"(.*)\t(.*)\tUTC(\D)(\d+):(\d+)", RegexOptions.IgnoreCase);
                if(!countryMatch.Success)
                {
                    Util.log($"Error reading country line {line}");
                    continue;
                }
                CountryTimeZone countryTimezone = new CountryTimeZone();
                countryTimezone.setCountry(countryMatch.Groups[1].Value);
                countryTimezone.countryCode = countryMatch.Groups[2].Value;
                countryTimezone.setTimezone(countryMatch.Groups[3].Value, countryMatch.Groups[4].Value, countryMatch.Groups[5].Value);

                countryTimezones.Add(countryTimezone);
            }
        }

        public bool getTimezone(string countryCode, out float timeZone)
        {
            foreach(CountryTimeZone tz in countryTimezones)
            {
                if(tz.countryCode == countryCode)
                {
                    timeZone = (float)(tz.timezoneOffset.TotalMinutes / 60.0);
                    return true;
                }
            }
            timeZone = 0;
            return false;
        }

    }
}
