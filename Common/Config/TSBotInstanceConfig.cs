﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Common.Config
{
    public class TSBotInstanceConfig
    {
        public string ip = "ts.dmg-inc.com";
        public string[] identity = null;
        public string[] divisions = null;
        public string defaultChannel = "/23209";
        public string username = "Kushzei_Bot";
        public string id = "default";
        public int channelNameUpdateRateMinutes = 20;
        public int channelDescUpdateRateMinutes = 5;
        public TSBotInstanceConfig()
		{

        }

    }
}
