﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class GlobalConfig
    {

        Dictionary<string, Object> values = new Dictionary<string, Object>();
        public GlobalConfig()
        {
 
        }
        //For internal use only
        public Dictionary<string, Object> getValues()
        {
            return values;
        }
        public void setConfig(string key, Object value)
        {
            if (values.ContainsKey(key))
                return;
            values.Add(key, value);
        }
        public Object getConfig(string key)
        {
            if (values.ContainsKey(key))
                return values[key];
            return 
                null;
        }
    }
}
