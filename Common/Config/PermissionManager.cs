﻿using Common.Config;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class PermissionManager
    {
        static PermissionManager permissionManager = new PermissionManager();
        public static PermissionManager get() { return permissionManager; }
        public delegate void UserEventHandler(object myObject,
                                          UserData userData);
        public UserEventHandler UserAdded;
        public struct Permission
        {
            public string name;
            public string value;
        }
        [Serializable()]
        public class UserData
        {
            public string tsUserUid;
            public string discordId = "";
            public uint division;
            public string countryCode;
            public HashSet<string> lastKnownNicknames = new HashSet<string>();
            public float timeZone;
            public List<Permission> permissions = new List<Permission>();
            public bool timezoneSet = false;
            public bool superAdmin = false;
            public bool talkMode = false;
            public string registerData = "";
            public uint listEventsDays = 7;
            public uint maxListEvents = 10;
            public uint listMyEventsDays = 7;
            public uint maxListMyEvents = 10;
            public bool hasPermission(string name, string value)
            {
                if (superAdmin)
                    return true;
                foreach(Permission p in permissions)
                {
                    if(p.name.Equals(name, StringComparison.InvariantCultureIgnoreCase)
                         && p.value.Equals(value, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return true;
                    }
                }
                return false;
            }
        }
        Dictionary<string,UserData> userDatas = new Dictionary<string,UserData>();
        public PermissionManager()
        {
            bool fileExists = File.Exists(getFilePath());
            if(!fileExists)
                writeFile();
            reload();
        }
        public void reload()
        {
            userDatas.Clear();
            readFile();
            writeFile();
        }
        public void readFile()
        {
            bool fileExists = File.Exists(getFilePath());
            if(!fileExists)
            {
                return;
            }
            using (System.IO.StreamReader file =
           new System.IO.StreamReader(getFilePath()))
            {
                JObject o = (JObject)JToken.ReadFrom(new JsonTextReader(file));
                if(o.Count != 1)
                {
                    Util.log("Error reading permissions! Invalid object count");
                    return;
                }
                JToken[] tokenArray = o.First.Values().ToArray();
                foreach(JToken token in tokenArray)
                {
                    UserData data = token.ToObject<UserData>();
                    userDatas.Add(data.tsUserUid, data);
                }
            }
        }
        public void addUser(string tsUid, string username)
        {
            if (userDatas.ContainsKey(tsUid))
            {
                if (!userDatas[tsUid].lastKnownNicknames.Contains(username))
                    userDatas[tsUid].lastKnownNicknames.Add(username);
                return;
            }
            UserData userData = new UserData();
            userData.tsUserUid = tsUid;
            userData.lastKnownNicknames.Add(username);

            UserAdded?.Invoke(this, userData);
            userDatas.Add(tsUid, userData);
            writeFile();
        }
        public void addPermission(string uid, string permissionName, string value)
        {
            if (!userDatas.ContainsKey(uid))
                return;
            UserData user = userDatas[uid];
            Permission permission = new Permission();
            permission.name = permissionName;
            permission.value = value;
            user.permissions.Add(permission);
            writeFile();
        }
        public void setDivision(string uid, uint division)
        {
            if (!userDatas.ContainsKey(uid))
                return;
            UserData user = userDatas[uid];
            user.division = division;
            writeFile();
        }
        public void setTimezone(string senderUid, int timezone)
        {
            if (!userDatas.ContainsKey(senderUid))
                return;
            UserData user = userDatas[senderUid];
            user.timeZone = timezone;
            user.timezoneSet = true;
            writeFile();
        }
        public UserData getUserByTsUid(string tsUid)
        {
            if (!userDatas.ContainsKey(tsUid))
                return null;
            return userDatas[tsUid];
        }
        public UserData getUserByDID(string discordId)
        {
            foreach(UserData d in userDatas.Values)
            {
                if (d.discordId == discordId)
                    return d;
            }
            return null;
        }
        public UserData getUserByRegisterData(string registerData)
        {
            foreach (UserData d in userDatas.Values)
            {
                if (d.registerData == registerData)
                    return d;
            }
            return null;
        }
        public string getFilePath()
        {
            return $"./{(string)ConfigManager.getGlobal().getConfig("permissionsFile")}";
        }
        public void writeFile()
        {
            bool fileExists = File.Exists(getFilePath());
            if (!fileExists)
            {
                FileStream stream = File.Create(getFilePath());
                stream.Close();
            }
            JArray array = new JArray();
            foreach (UserData u in userDatas.Values)
            {
                array.Add(JObject.FromObject(u));
            }

            JObject o = new JObject();
            o["UserArray"] = array;

            string json = o.ToString();
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(getFilePath(), false))
            {
                file.WriteLine(json);
            }
        }

        
    }
}
